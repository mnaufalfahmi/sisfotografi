-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2021 at 08:54 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sis_fotografis`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) DEFAULT NULL,
  `trx_code` varchar(25) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `name_user` varchar(50) NOT NULL,
  `email_user` varchar(50) NOT NULL,
  `username_user` varchar(50) NOT NULL,
  `telp_user` varchar(16) DEFAULT NULL,
  `role_user` enum('customers','admins','super_admins','anonymous') NOT NULL,
  `message` text,
  `name_file` varchar(100) DEFAULT NULL,
  `type_file` varchar(25) DEFAULT NULL,
  `size_file` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_services`
--

CREATE TABLE `tbl_services` (
  `id` int(11) NOT NULL,
  `service_category_id` int(11) DEFAULT NULL,
  `name_service_category` varchar(50) DEFAULT NULL,
  `service_code` varchar(50) NOT NULL,
  `name_service` varchar(50) DEFAULT NULL,
  `description` text,
  `images` text,
  `price` int(11) DEFAULT NULL,
  `on_slider` enum('yes','no') DEFAULT 'no',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  `deleted` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_services`
--

INSERT INTO `tbl_services` (`id`, `service_category_id`, `name_service_category`, `service_code`, `name_service`, `description`, `images`, `price`, `on_slider`, `created_on`, `modified_on`, `deleted`) VALUES
(3, 4, 'Cetak Photo', 'BINGKAI1R', 'Bingkai 1 R', NULL, '', 10000, 'no', '2021-05-31 03:43:26', NULL, 0),
(4, 4, 'Cetak Photo', 'BINGKAI2R', 'Bingkai 2 R', '', '14.png', 20000, 'no', '2021-05-31 04:33:38', NULL, 0),
(5, 4, 'Cetak Photo', 'BINGKAI3R', 'Bingkai 3 R', NULL, '4.png', 40000, 'no', '2021-05-31 03:58:05', NULL, 0),
(8, 6, 'Pas Foto White', 'fotovisa', 'Paket Foto VISA', '- 2 lembar untuk 1 ukuran\r\n- 1 CD file foto', '5.png', 65000, 'yes', '2021-05-31 06:33:47', NULL, 0),
(9, 6, 'Pas Foto White', 'fotohajiumroh', 'Paket Foto Haji Umroh', '- 2 lembar untuk 1 ukuran\r\n- 1 CD file foto', 'slider.png', 65000, 'yes', '2021-05-31 06:33:44', NULL, 0),
(18, 6, 'Pas Foto White', 'fotonikah', 'Paket Foto Nikah Berdampingan', '- 2 Ukuran untuk 1 ukuran / 6 x 2 = 2 Lembar\r\n- 1 CD file foto', 'slider.png', 65000, 'no', '2021-05-31 03:59:22', NULL, 0),
(19, 6, 'Pas Foto White', 'backgroundputih', 'Pas Foto Background Putih', '- 2 lembar untuk 1 ukuran\r\n- 1 CD file foto', 'slider.png', 65000, 'no', '2021-05-31 03:59:22', NULL, 0),
(20, 6, 'Pas Foto White', 'asdkas1123', 'tesiasad', 'asda', '14.png', 90000, 'no', '2021-05-31 04:33:25', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_category`
--

CREATE TABLE `tbl_service_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_service_category`
--

INSERT INTO `tbl_service_category` (`id`, `name`, `code`, `created_on`, `modified_on`, `deleted`) VALUES
(1, 'Bingkai foto preweding io', 'Bingkai_foto_preweding_io', '2021-03-26 09:21:13', NULL, 1),
(2, 'Aksesoris Camera bagus sekali', 'Aksesoris_Camera_bagus_sekali', '2021-03-26 09:21:15', NULL, 1),
(3, 'bingkaiana', 'bingkaiana', '2021-03-26 09:21:18', NULL, 1),
(4, 'Cetak Photo', 'Cetak_Photo', '2021-03-26 09:18:57', NULL, 0),
(5, 'Cetak Visa', 'Cetak_Visa', '2021-03-26 09:19:04', NULL, 0),
(6, 'Pas Foto White', 'Pas_Foto_White', '2021-04-01 09:55:27', NULL, 0),
(7, 'Foto Wisuda', 'Foto_Wisuda', '2021-04-01 09:56:26', NULL, 0),
(8, 'Foto CLose Up', 'Foto_CLose_Up', '2021-04-01 09:56:49', NULL, 0),
(9, 'Foto Keluarga', 'Foto_Keluarga', '2021-04-01 09:56:58', NULL, 0),
(10, 'Pas Foto ID', 'Pas_Foto_ID', '2021-04-01 09:57:12', NULL, 0),
(11, 'ss  sa', 'ss__sa', '2021-04-03 08:50:49', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactions`
--

CREATE TABLE `tbl_transactions` (
  `id` int(11) NOT NULL,
  `trx_code` varchar(25) DEFAULT NULL,
  `qty` int(11) DEFAULT '1',
  `base_price` int(11) NOT NULL DEFAULT '0' COMMENT 'nominal yang di transfer',
  `id_user` int(11) NOT NULL,
  `name_user` varchar(50) NOT NULL,
  `email_user` varchar(50) NOT NULL,
  `username_user` varchar(50) NOT NULL,
  `telp_user` varchar(16) NOT NULL,
  `status` enum('success','failed','canceled','progress') NOT NULL DEFAULT 'progress',
  `status_payment` varchar(50) NOT NULL DEFAULT 'pending',
  `service_id` int(11) NOT NULL,
  `service_code` varchar(50) NOT NULL COMMENT 'jenis service yang di ambil',
  `service_name` varchar(50) NOT NULL COMMENT 'nama service yang di ambil',
  `remarks` text,
  `payments` enum('online','on_location') DEFAULT NULL COMMENT 'pembayaran lewat atm/tunai/digital payment',
  `start_booking` datetime DEFAULT NULL COMMENT 'jadwal awal pemesanan',
  `end_booking` datetime DEFAULT NULL COMMENT 'jadwal akhir pemesanan',
  `evidence_transfer` text COMMENT 'bukti pembayaran',
  `status_evidence_transfer` enum('accepted','rejected','waiting','none') DEFAULT 'none',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_transactions`
--

INSERT INTO `tbl_transactions` (`id`, `trx_code`, `qty`, `base_price`, `id_user`, `name_user`, `email_user`, `username_user`, `telp_user`, `status`, `status_payment`, `service_id`, `service_code`, `service_name`, `remarks`, `payments`, `start_booking`, `end_booking`, `evidence_transfer`, `status_evidence_transfer`, `created_on`, `modified_on`, `deleted`) VALUES
(64, 'TRX210531113217', 1, 10000, 1, 'Solahudin', 'solahudin@gmail.com', 'solahudin', '0895414724178', 'progress', 'settlement', 3, 'BINGKAI1R', 'Bingkai 1 R', 'bukti pembayaran sudah benar, sekarang sedang  di kerjakansadadsada untuk filenya tolong di lampirkan s asda', 'on_location', NULL, NULL, 'assets/transactions/bukti_pembayaran/-I GUSTI AYU MADE SAWITRI, A.Md.Kes (2105160299).pdf', 'accepted', '2021-05-31 09:51:02', NULL, 0),
(65, 'TRX210531113648', 12, 780000, 1, 'Solahudin', 'solahudin@gmail.com', 'solahudin', '0895414724178', 'progress', 'settlement', 8, 'fotovisa', 'Paket Foto VISA', 'sukses ts', 'on_location', NULL, NULL, 'assets/transactions/bukti_pembayaran/-Data Faskes RS AWAL BROS.xls', 'accepted', '2021-05-31 09:51:34', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction_documents`
--

CREATE TABLE `tbl_transaction_documents` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `trx_code` varchar(25) NOT NULL,
  `service_code` varchar(50) DEFAULT NULL,
  `service_name` varchar(50) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `name_user` varchar(50) NOT NULL,
  `name_file` varchar(50) DEFAULT NULL,
  `path_file` text,
  `status` enum('raw_customers','finished','evidence_transfer') NOT NULL DEFAULT 'raw_customers' COMMENT 'raw customer = dokumen dari customer, finished = dokumen sudah di kerjakan oleh admin',
  `remarks` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_transaction_documents`
--

INSERT INTO `tbl_transaction_documents` (`id`, `trx_id`, `trx_code`, `service_code`, `service_name`, `id_user`, `name_user`, `name_file`, `path_file`, `status`, `remarks`, `created_on`, `modified_on`, `deleted`) VALUES
(91, 64, 'TRX210531113217', 'BINGKAI1R', 'Bingkai 1 R', 1, 'Solahudin', '', 'assets/transactions/document/checkout/', 'raw_customers', NULL, '2021-05-31 09:32:17', NULL, 0),
(92, 64, 'TRX210531113217', 'BINGKAI1R', NULL, 1, 'Solahudin', 'TRX210531113217-Lhu-2104120070.pdf', 'assets/transactions/bukti_pembayaran/TRX210531113217-Lhu-2104120070.pdf', 'evidence_transfer', NULL, '2021-05-31 09:32:25', NULL, 0),
(93, 64, 'TRX210531113217', 'BINGKAI1R', NULL, 2, 'Boy Andreas', '-', 'assets/transactions/bukti_pembayaran/-', 'evidence_transfer', NULL, '2021-05-31 09:33:25', NULL, 0),
(94, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 1, 'Solahudin', 'patient-list-swab (1).xls', 'assets/transactions/document/checkout/patient-list-swab (1).xls', 'raw_customers', NULL, '2021-05-31 09:36:48', NULL, 0),
(95, 65, 'TRX210531113648', 'fotovisa', NULL, 2, 'Boy Andreas', '-Data Faskes RSU ISLAM FAISAL MAKASSAR.xls', 'assets/transactions/bukti_pembayaran/-Data Faskes RSU ISLAM FAISAL MAKASSAR.xls', 'evidence_transfer', NULL, '2021-05-31 09:47:15', NULL, 0),
(96, 65, 'TRX210531113648', 'fotovisa', NULL, 2, 'Boy Andreas', '-', 'assets/transactions/bukti_pembayaran/-', 'evidence_transfer', NULL, '2021-05-31 09:47:40', NULL, 0),
(97, 64, 'TRX210531113217', 'BINGKAI1R', NULL, 2, 'Boy Andreas', '-Data Faskes RSU ISLAM FAISAL MAKASSAR (1).xls', 'assets/transactions/bukti_pembayaran/-Data Faskes RSU ISLAM FAISAL MAKASSAR (1).xls', 'evidence_transfer', NULL, '2021-05-31 09:49:41', NULL, 0),
(98, 64, 'TRX210531113217', 'BINGKAI1R', NULL, 2, 'Boy Andreas', '-Data Faskes RSU ISLAM FAISAL MAKASSAR (1).xls', 'assets/transactions/bukti_pembayaran/-Data Faskes RSU ISLAM FAISAL MAKASSAR (1).xls', 'evidence_transfer', NULL, '2021-05-31 09:49:41', NULL, 0),
(99, 64, 'TRX210531113217', 'BINGKAI1R', NULL, 2, 'Boy Andreas', '-I GUSTI AYU MADE SAWITRI, A.Md.Kes (2105160299).p', 'assets/transactions/bukti_pembayaran/-I GUSTI AYU MADE SAWITRI, A.Md.Kes (2105160299).pdf', 'evidence_transfer', NULL, '2021-05-31 09:50:34', NULL, 0),
(100, 64, 'TRX210531113217', 'BINGKAI1R', NULL, 2, 'Boy Andreas', '-I GUSTI AYU MADE SAWITRI, A.Md.Kes (2105160299).p', 'assets/transactions/bukti_pembayaran/-I GUSTI AYU MADE SAWITRI, A.Md.Kes (2105160299).pdf', 'evidence_transfer', NULL, '2021-05-31 09:50:52', NULL, 0),
(101, 64, 'TRX210531113217', 'BINGKAI1R', NULL, 2, 'Boy Andreas', '-I GUSTI AYU MADE SAWITRI, A.Md.Kes (2105160299).p', 'assets/transactions/bukti_pembayaran/-I GUSTI AYU MADE SAWITRI, A.Md.Kes (2105160299).pdf', 'evidence_transfer', NULL, '2021-05-31 09:50:52', NULL, 0),
(102, 65, 'TRX210531113648', 'fotovisa', NULL, 2, 'Boy Andreas', '-Data Faskes RS AWAL BROS.xls', 'assets/transactions/bukti_pembayaran/-Data Faskes RS AWAL BROS.xls', 'evidence_transfer', NULL, '2021-05-31 09:51:34', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction_services`
--

CREATE TABLE `tbl_transaction_services` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `trx_code` varchar(25) NOT NULL,
  `service_code` varchar(50) NOT NULL,
  `service_name` varchar(50) NOT NULL,
  `service_price` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_transaction_services`
--

INSERT INTO `tbl_transaction_services` (`id`, `trx_id`, `trx_code`, `service_code`, `service_name`, `service_price`, `created_on`, `modified_on`, `deleted`) VALUES
(297, 64, 'TRX210531113217', 'BINGKAI1R', 'Bingkai 1 R', 10000, '2021-05-31 09:32:17', NULL, 0),
(298, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(299, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(300, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(301, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(302, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(303, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(304, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(305, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(306, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(307, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(308, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0),
(309, 65, 'TRX210531113648', 'fotovisa', 'Paket Foto VISA', 65000, '2021-05-31 09:36:48', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `gender` enum('male','female','other') DEFAULT 'other',
  `telp` varchar(16) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `role` enum('customers','admins','super_admins','anonymous') NOT NULL,
  `birthday` date DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `username`, `password`, `email`, `gender`, `telp`, `status`, `role`, `birthday`, `created_on`, `modified_on`, `deleted`) VALUES
(1, 'Solahudin', 'solahudin', '21232f297a57a5a743894a0e4a801fc3', 'solahudin@gmail.com', 'male', '0895414724178', 'active', 'customers', '1998-02-17', '2020-12-17 13:31:10', NULL, 0),
(2, 'Boy Andreas', 'boyandreas', '21232f297a57a5a743894a0e4a801fc3', 'boyandreas@gmail.com', 'male', '0895414724178', 'active', 'admins', '1998-02-17', '2020-12-17 13:31:10', NULL, 0),
(3, 'anonymous', 'anonymous', NULL, 'anonymous@mail.com', 'male', NULL, 'active', 'anonymous', NULL, '2020-12-17 13:31:10', NULL, 0),
(6, 'solah', 'solah', '25f9e794323b453885f5181f1b624d0b', 'solah@gmail.com', 'female', '0895414724178', 'active', 'customers', '2020-12-22', '2020-12-22 03:59:19', NULL, 0),
(7, 'bang fahmi', 'naufalfahmie23', '0cc175b9c0f1b6a831c399e269772661', 'naufalfahmie23@gmail.com', 'male', '0895414724178', 'active', 'customers', '2021-05-01', '2021-05-16 02:49:18', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tbl_services`
--
ALTER TABLE `tbl_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_category_id` (`service_category_id`);

--
-- Indexes for table `tbl_service_category`
--
ALTER TABLE `tbl_service_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `tbl_transaction_documents`
--
ALTER TABLE `tbl_transaction_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transaction_services`
--
ALTER TABLE `tbl_transaction_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_services`
--
ALTER TABLE `tbl_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_service_category`
--
ALTER TABLE `tbl_service_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `tbl_transaction_documents`
--
ALTER TABLE `tbl_transaction_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `tbl_transaction_services`
--
ALTER TABLE `tbl_transaction_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
