<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrations extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		$this->template->load('templates/home_template','home_/home');
	}

	public function save()
	{
		$username = explode('@',$this->input->post('email'));
		$check = $this->db->get_where('tbl_users',array('email'=>$this->input->post('email')))->num_rows();

		$data = array(
			'name'     => $this->input->post('name'),
			'username' => $username[0],
			'telp'     => $this->input->post('phone'),
			'email'    => $this->input->post('email'),
			'gender'   => $this->input->post('gender'),
			'birthday' => $this->input->post('birthday'),
			'password' => md5($this->input->post('password')),
			'status'   => 'active',
			'role'	   => 'customers',
		);
		try {
			//code...
			if($check > 0) {
				$message['notification'] = "email yang anda masukkan sudah terdaftar dalam sistem. silahkan gunakan email yang lain";
				$this->template->load('templates/home_template','home_/callback/registrations/failed',$message);
			} else {
				$this->db->insert('tbl_users',$data);
				$message['notification'] = "akun anda sudah terdaftar. silahkan masuk dengan email dan dan password di halaman login";
				$this->template->load('templates/home_template','home_/callback/registrations/success',$message);
			}

		} catch (\Throwable $th) {
			//throw $th;
			$message['notification'] = "akun anda gagal dibuat. pastikan semua data yang anda masukkan sudah benar";
			$this->template->load('templates/home_template','home/callback/registrations/failed',$message);
		}
	}
}
