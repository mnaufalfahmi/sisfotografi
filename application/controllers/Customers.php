<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		authentication($this);
		$this->load->model("model_customers","customers");		
	}

	public function index()
	{
		// untuk mengambil data session login
		$data=dashboard_data($this);
		$this->template->load('templates/dashboard_template','dashboard/customers/index',$data);
	}

	public function datatables()
	{
		$list = $this->customers->get_datatables();

		$data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();

            $row['no']         = $no;
            $row['name']       = $l->name;
            $row['email']      = $l->email;
            $row['gender']     = $l->gender;
            $row['telp']       = $l->telp;
            $row['status']     = $l->status;
            $row['created_on'] = $l->created_on;
            
            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->customers->count_all(),
            "recordsFiltered"   => $this->customers->count_filtered(),
            "data"              => $data,
        );

        //output to json format
        echo json_encode($output);
	}
}
