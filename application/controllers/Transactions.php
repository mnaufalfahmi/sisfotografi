<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		authentication($this);
		$this->load->model("model_transactions","transactions");

        $params = array('server_key' => 'SB-Mid-server-KrZsmVCF9uTyj4TmLZt60Seh', 'production' => false);
		$this->load->library('midtrans');
		$this->load->library('veritrans');
		$this->midtrans->config($params);
		$this->veritrans->config($params);
		$this->load->helper('url');
	}

	public function index()
	{
		// untuk mengambil data session login
        if($this->session->userdata('role') == 'customers')
        {
            $transactions = $this->db->order_by('id','desc')->get_where('tbl_transactions',['deleted' => 0, 'id_user' => $this->session->userdata('id')])->result();
            $progress = $this->transactions->get_transaction_documents();

            $data['transactions'] = $transactions;
            $data['transactions_progress'] = $progress;
            $this->template->load('templates/home_template','home_/transactions/index',$data);

        } else{
            $data=dashboard_data($this);
            redirect('transactions/all',$data);
            // $this->template->load('templates/dashboard_template','dashboard/dashboard',$data);
        }
	}

    // from customers
    public function submitDocuments()
	{
        $trx_code = $this->input->post('trx_code');
        $checking = $this->db->get_where('tbl_transactions',['trx_code' => $trx_code])->row();

		if(!empty($_FILES['upload_files']))
		{
			$tmp_name = $_FILES["upload_files"]["tmp_name"];
			$name     = $_FILES["upload_files"]["name"];
            $nameFix  = $trx_code.'-'.$name;
			$path     = "assets/transactions/bukti_pembayaran/".$nameFix;
			move_uploaded_file($tmp_name, $path);

			$dataDocuments = [
					'trx_id'       => $checking->id,
					'trx_code'     => $trx_code,
					'service_code' => $checking->service_code,
					'service_name' => $checking->name_service,
					'id_user'      => $checking->id_user,
					'name_user'    => $checking->name_user,
					'name_file'    => $nameFix,
					'path_file'    => $path,
					'status'       => 'evidence_transfer'
			];

            $this->db->insert('tbl_transaction_documents',$dataDocuments);

            $dataDocumentsTransactions = [
                'status_evidence_transfer' => 'waiting',
                'evidence_transfer'        => $path,
            ];

            $this->db->where('trx_code',$trx_code);
			$this->db->update('tbl_transactions',$dataDocumentsTransactions);

            redirect('transactions');
		} else {
            // redirect('transactions');
        }
	}

    public function details($trx_code = null)
	{
		// untuk mengambil data session login
        if($this->session->userdata('role') == 'customers')
        {
            $transactions                 = $this->db->get_where('tbl_transactions',['deleted' => 0,'id_user' => $this->session->userdata('id')])->result();
            $transaction_details          = $this->db->get_where('tbl_transactions',['deleted' => 0,'trx_code' => $trx_code])->row();
            $transaction_document_details = $this->db->get_where('tbl_transaction_documents',['deleted' => 0,'trx_code' => $trx_code])->result();
            $transaction_services_details = $this->db->get_where('tbl_transaction_services',['deleted' => 0,'trx_code' => $trx_code])->result();

            $data['transactions']                 = $transactions;
            $data['transaction_details']          = $transaction_details;
            $data['transaction_document_details'] = $transaction_document_details;
            $data['transaction_services_details'] = $transaction_services_details;
            
            $this->template->load('templates/home_template','home/transactions/detail',$data);

        } else{
            $data=dashboard_data($this);
            $this->template->load('templates/dashboard_template','dashboard/dashboard',$data);
        }
	}

	public function datatables()
	{
        $list = $this->transactions->get_datatables();

		$data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();

            $row['no']                       = $no;
            $row['trx_code']                 = $l->trx_code;
            $row['status']                   = $l->status;
            $row['base_price']               = $l->base_price;
            $row['name_user']                = $l->name_user;
            $row['telp_user']                = $l->telp_user;
            $row['service_code']             = $l->service_code;
            $row['service_name']             = $l->service_name;
            $row['payments']                 = (($l->payments == 'on_location') ? 'Di lokasi' : $l->payments);
            $row['status_payment']           = $l->status_payment;
            $row['remarks']                  = $l->remarks;
            $row['evidence_transfer']        = $l->evidence_transfer;
            $row['status_evidence_transfer'] = $l->status_evidence_transfer;
            // $row['status_document']          = $l->status_evidence_transfer;
            $row['created_on']               = $l->created_on;
            $row['action']            = '<button data-id="'.$l->id.'" type="button" class="btn btn-success btn-update btn-xs" data-toggle="tooltip" title="edit"><i class="fa fa-pencil"></i></button>
                                        <button data-id-progress="'.$l->id.'" type="button" class="btn btn-info btn-progress btn-xs" data-toggle="tooltip" title="progress to customers"><i class="fa fa-exchange"></i></button>
                                        <a target="_blank" href="'.base_url().'transactions/invoice/'.$l->id.'" class="btn btn-default btn-prints btn-xs" data-toggle="tooltip" title="print"><i class="fa fa-print"></i></a>';
            
            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->transactions->count_all(),
            "recordsFiltered"   => $this->transactions->count_filtered(),
            "data"              => $data,
        );

        //output to json format
        echo json_encode($output);
	}

    public function invoice($id = null)
    {
        $arr['data'] = $this->db->get_where('tbl_transactions',['id' => $id])->row();

        $this->db->select('a.*, b.description')->from('tbl_transaction_services a')->join('tbl_services b','b.service_code = a.service_code')->where('a.trx_id',$id);
        $service = $this->db->get()->result();
        $arr['service'] = $service;
        $this->load->view('invoice', $arr);
    }

	public function all()
	{
		$data=dashboard_data($this);
		$this->template->load('templates/dashboard_template','dashboard/transactions/index',$data);	
	}

    function status($trx_code)
	{
		$status = $this->veritrans->status($trx_code);
		return $status->transaction_status;
	}

    public function edit()
    {
        $id              = $this->input->post('id');
        $trx_code        = $this->input->post('trx_code');
        $status_payment  = $this->input->post('status_payment');
        $type_payment    = $this->input->post('type_payment');
        $remarks         = $this->input->post('remarks');
        $status_evidence = $this->input->post('status_evidence_transfer');

        try {
            
            $data = [
                // 'payments'                 => $type_payment,
                'status_payment'           => $status_payment,
                'remarks'                  => $remarks,
                'status_evidence_transfer' => $status_evidence,
            ];

            $checking = $this->db->get_where('tbl_transactions',['id' => $id])->row();
            
            if(!empty($_FILES['file']["name"]))
            {
                $tmp_name = $_FILES["file"]["tmp_name"];
                $name     = $_FILES["file"]["name"];
                $nameFix  = $trx_code.'-'.$name;
                $path     = "assets/transactions/bukti_pembayaran/".$nameFix;
                move_uploaded_file($tmp_name, $path);


                $dataDocuments = [
                    'trx_code'     => $checking->trx_code,
                    'trx_id'       => $id,
                    'service_code' => $checking->service_code,
                    'service_name' => $checking->name_service,
                    'id_user'      => $this->session->userdata('id'),
                    'name_user'    => $this->session->userdata('name'),
                    'name_file'    => $nameFix,
                    'path_file'    => $path,
                    'status'       => 'evidence_transfer',
                ];

                $this->db->insert('tbl_transaction_documents',$dataDocuments);

                $data['evidence_transfer'] = $path;
            }

            // var_dump($data);
            $this->db->where('id',$id);
            $this->db->update('tbl_transactions',$data);

        } catch (\Throwable $th) {
            
        }

    }

    public function editProgress()
    {
        $id     = $this->input->post('id');
        $status = $this->input->post('workmanship_status');
        $remarks = $this->input->post('remarks');

        try {

            $data = [
                'status' => $status,
            ];

            $checking = $this->db->get_where('tbl_transactions',['id' => $id])->row();
            
            if(!empty($_FILES['fileProgress']["name"]))
            {
                $tmp_name = $_FILES["fileProgress"]["tmp_name"];
                $name     = $_FILES["fileProgress"]["name"];
                $nameFix  = $checking->trx_code.'-'.$name;
                $path     = "assets/transactions/document/finished/".$nameFix;
                move_uploaded_file($tmp_name, $path);
                
                
                $dataDocuments = [
                    'trx_code'     => $checking->trx_code,
                    'trx_id'       => $id,
                    'service_code' => $checking->service_code,
                    'service_name' => $checking->name_service,
                    'id_user'      => $this->session->userdata('id'),
                    'name_user'    => $this->session->userdata('name'),
                    'name_file'    => $nameFix,
                    'path_file'    => $path,
                    'status'       => 'finished',
                    'remarks'      => $remarks,
                ];
                
                $this->db->insert('tbl_transaction_documents',$dataDocuments);

            }

            // var_dump($data);
            $this->db->where('id',$id);
            $this->db->update('tbl_transactions',$data);

        } catch (\Throwable $th) {
            
        }

    }
}
