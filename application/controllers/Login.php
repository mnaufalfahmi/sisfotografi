<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model("model_login");
	}

	public function index()
	{
		if($this->session->userdata('user_logged_in') == true) {
			redirect(base_url());
		} else {
			$this->template->load('templates/home_template','home_/login');
		}
	}

	public function verification()
	{
		$email= $this->input->post('email');
		$password= $this->input->post('password');

		$user_login_data=array(
			'email'    => $email,
			'password' => md5($password)
		);

		$do_verify=$this->model_login->verification($user_login_data)->num_rows();
		if($do_verify>0)
		{
			$user_data=$this->model_login->get_veified_user_data($email);
			foreach($user_data as $data)
			{
				$data_session=array(
					 "id"  		=> $data['id'],
					 "name"     => $data['name'],
					 "email"    => $data['email'],
					 "gender"   => $data['gender'],
					 "telp"     => $data['telp'],
					 "status"   => $data['status'],
					 "username" => $data['username'],
					 "role"     => $data['role'],
					 "status"	=> "login"
				);
			}
			$this->session->set_userdata($data_session);
            $this->session->set_userdata('user_logged_in', true);


			if($this->session->userdata('role') == 'customers'){
				redirect(base_url());
			} else {
				redirect(base_url('dashboard'));
			}
		}
		else
		{
			echo "<script>
			alert('email dan password anda salah !, Silahkan coba lagi.');
			window.location.href='".base_url('login')."';
			</script>";
		}

	}

	public function signout()
	{
		if($this->session->userdata('role') == 'customers'){
            $this->session->set_userdata('user_logged_in', false);
			$this->session->sess_destroy();
			redirect(base_url());
		} else {
            $this->session->set_userdata('user_logged_in', false);
			$this->session->sess_destroy();
			redirect(base_url('login'));
		}
	}
}
