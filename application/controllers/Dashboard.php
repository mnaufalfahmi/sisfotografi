<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		authentication($this);
		$this->load->model("Model_dashboard","dashboard");
	}

	public function index()
	{
		// untuk mengambil data session login
		$data=dashboard_data($this);
		$data['counting'] = $this->dashboard->counting();
		$this->template->load('templates/dashboard_template','dashboard/dashboard',$data);
	}
}
