<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$params = array('server_key' => 'SB-Mid-server-KrZsmVCF9uTyj4TmLZt60Seh', 'production' => false);
		$this->load->library('midtrans');
		$this->load->library('veritrans');
		$this->midtrans->config($params);
		$this->veritrans->config($params);
		$this->load->helper('url');
	}

	public function index()
	{	
		$data['category_services'] = $this->db->get_where('tbl_service_category',['deleted' => 0])->result();
		$data['services']  		   = $this->db->get_where('tbl_services',['deleted' => 0])->result();
		$this->template->load('templates/home_template','home_/home',$data);
	}

	public function contact()
	{	
		$this->template->load('templates/home_template','home_/contact/index');
	}

	public function category($paket = null)
	{

		$categoryService = $this->db->get_where('tbl_service_category',['deleted' => 0, 'code' => $paket]);
		$categoryServiceRows = $categoryService->row();
		$services = $this->db->get_where('tbl_services',['deleted' => 0,'service_category_id' => (int) $categoryServiceRows->id])->result();
		
		if($paket == null)
		{
			redirect('home');
		}

		if($categoryService->num_rows() < 1)
		{
			redirect('home');
		}

		$data['category_services'] = $this->db->get_where('tbl_service_category',['deleted' => 0])->result();
		$data['services']  		   = $services;
		$this->template->load('templates/home_template','home_/main', $data);
	}	

	public function cart($serviceCode)
	{

		if($serviceCode == null)
		{
			redirect('home');
		}
		
		$data['services'] = $this->db->get_where('tbl_services',['deleted' => 0,'service_code' => $serviceCode])->row();
		$this->template->load('templates/home_template','home_/cart/index',$data);

	}

	public function generate()
	{
		$generate = 'TRX'.date('ymdhis');
		return $generate;
	}

	function status()
	{
		$order_id = $this->generate();
		$status = $this->veritrans->status($order_id);
		return $status->transaction_status;
	}

	public function checkoutOnline()
	{	

		$segmentService = $this->input->post('paket');
		$qty       = $this->input->post('qty');
		$img       = $this->input->post('img');
		$payment   = $this->input->post('payment');
		$total     = $this->input->post('price');
		$expName   = explode(' ',$this->session->userdata('name'));


		$serviceID = $this->db->get_where('tbl_services',['service_code' => $segmentService])->row();
		$ids       = $serviceID->id;
		$checking  = $serviceID;


		if(empty($ids)){
			$this->rest->send_error(404,'Paket not found');
			exit;
		}

		if(empty($qty)){
			$this->rest->send_error(404,'qty not found');
			exit;
		}

		// if(empty($img)){
		// 	$this->rest->send_error(404,'Files not found');
		// 	exit;
		// }

		if(empty($payment)){
			$this->rest->send_error(404,'Payment not found');
			exit;
		}

		
		// $lastID   = $this->db->order_by('id', 'desc')->get_where('tbl_services',['id' => $ids], 1)->row();

		$data = [
			'trx_code' 		=> $this->generate(),
			'service_code'  => $checking->service_code,
			'service_name'  => $checking->name_service,
			'service_price' => $checking->price
		];

		$dataTransactions = [
			'trx_code'		=> $this->generate(),
			'base_price'    => ((int) $checking->price * $qty),
			'id_user'       => $this->session->userdata('id'),
			'name_user'     => $this->session->userdata('name'),
			'email_user'    => $this->session->userdata('email'),
			'username_user' => $this->session->userdata('username'),
			'telp_user'     => $this->session->userdata('telp'),
			'status'        => 'progress',
			'service_id'    => $checking->id,
			'service_code'  => $checking->service_code,
			'service_name'  => $checking->name_service,
			'remarks'       => 'pemesanan awal',
			'payments'      => $payment,
			'status_payment'=> 'pending',
			'qty'		    => $qty,
		];
		
		$this->db->insert('tbl_transactions',$dataTransactions);
		$lastTransaction = $this->db->insert_id();
		
		if(!empty($_FILES['img']))
			{
				$tmp_name = $_FILES["img"]["tmp_name"];
				$name = $_FILES["img"]["name"];
				$path = "assets/transactions/document/checkout/".$name;
				move_uploaded_file($tmp_name, $path);

				$dataDocuments = [
					'trx_id'       => $lastTransaction,
					'trx_code'     => 'TRX'.date('ymdhis'),
					'service_code' => $checking->service_code,
					'service_name' => $checking->name_service,
					'id_user'      => $this->session->userdata('id'),
					'name_user'    => $this->session->userdata('name'),
					'name_file'    => $name,
					'path_file'    => $path,
					'status'       => 'raw_customers'
				];

				$this->db->insert('tbl_transaction_documents',$dataDocuments);
			}
			

			for ($i=0; $i < $qty; $i++) {
				$data['trx_id'] = $lastTransaction;	
				$this->db->insert('tbl_transaction_services',$data);
			}

		// Required
		$transaction_details = array(
			'order_id'     => $this->generate(),
			'gross_amount' => ( (int)$checking->price * $qty),   // no decimal allowed for creditcard
		  );
	
		$arr = [
			'id'       => $checking->service_code,
			'price'    => (int)$checking->price,
			'quantity' => $qty,
			'name'     => $checking->name_service
		];
  
		  // Optional
		  $item_details = $arr;
  
		  // Optional
		  $billing_address = array(
			'first_name'    => $expName[0],
			'last_name'     => (empty(@$expName[1]) ? ' ' : @$expName[1]),
			'address'       => 'bogor',
			'city'          => 'bogor',
			'postal_code'   => "16602",
			'phone'         => $this->session->userdata('telp'),
			'country_code'  => 'IDN'
		  );
   
		  // Optional
		  $customer_details = array(
			'first_name'      => $expName[0],
			'last_name'     => (empty(@$expName[1]) ? ' ' : @$expName[1]),
			'email'           => $this->session->userdata('email'),
			'phone'           => $this->session->userdata('phone'),
			'billing_address' => $billing_address,
			// 'shipping_address' => $shipping_address
		  );
  
		  // Data yang akan dikirim untuk request redirect_url.
		  $credit_card['secure'] = true;
		  //ser save_card true to enable oneclick or 2click
		  //$credit_card['save_card'] = true;
  
		  $time = time();
		  $custom_expiry = array(
			  'start_time' => date("Y-m-d H:i:s O",$time),
			  'unit' => 'minute', 
			  'duration'  => 1440
		  );
		  
		  $transaction_data = array(
			  'transaction_details'=> $transaction_details,
			  'item_details'       => $item_details,
			  'customer_details'   => $customer_details,
			  'credit_card'        => $credit_card,
			  'expiry'             => $custom_expiry
		  );
  
		// $this->rest->send($transaction_data);exit;
		//   $this->session->set_flashdata('messages','Transaksi pemesanan anda pending. segera lakukan pembayaran agar dapat di proses ke tahap selanjutnya');

		  error_log(json_encode($transaction_data));
		  $snapToken = $this->midtrans->getSnapToken($transaction_data);
		  error_log($snapToken);
		  echo $snapToken;
	}

	public function price()
	{
		$serviceID  = $this->input->post('serviceid');
		try {
			//code...
			$services = $this->db->get_where('tbl_services',['id'=>$serviceID,'deleted' => 0])->row();
			echo json_encode($services->price);
		} catch (\Throwable $th) {
			//throw $th;
			echo json_encode('maaf sedang ada kendala teknis. silahkan coba beberapa saat lagi.');
		}
	}

	public function submitDocuments()
	{
			if(!empty($_FILES['img']))
			{
				$tmp_name = $_FILES["img"]["tmp_name"];
				$name = $_FILES["img"]["name"];
				$path = "assets/transactions/document/checkout/".$name;
				move_uploaded_file($tmp_name, $path);

				$dataDocuments = [
					'trx_id'       => $lastTransaction,
					'trx_code'     => 'TRX'.date('ymdhis'),
					'service_code' => $checking->service_code,
					'service_name' => $checking->name_service,
					'id_user'      => $this->session->userdata('id'),
					'name_user'    => $this->session->userdata('name'),
					'name_file'    => $name,
					'path_file'    => $path,
					'status'       => 'raw_customers'
				];

				$this->db->insert('tbl_transaction_documents',$dataDocuments);
			}	
	}

	public function submit()
	{
		$serviceID = $this->input->post('paket');
		$qty       = $this->input->post('qty');
		$img       = $this->input->post('img');
		$payment   = $this->input->post('payment');
		$total     = $this->input->post('price');
		

		if($this->session->userdata('user_logged_in') == false){
			echo json_encode('session has expired.');
			exit;
		}

		if(empty($serviceID)){
			$this->rest->send_error(404,'Paket not found');
			exit;
		}

		if(empty($qty)){
			$this->rest->send_error(404,'qty not found');
			exit;
		}

		// if(empty($img)){
		// 	$this->rest->send_error(404,'Files not found');
		// 	exit;
		// }

		if(empty($payment)){
			$this->rest->send_error(404,'Payment not found');
			exit;
		}

		// $checking = $this->db->get_where('tbl_services',['id' => $serviceID])->row();

		$serviceID = $this->db->get_where('tbl_services',['service_code' => $serviceID])->row();
		$ids       = $serviceID->id;
		$checking  = $serviceID;

		$lastID   =  $this->db->order_by('id', 'desc')->get_where('tbl_services',['id' => $ids], 1)->row();

		$data = [
			'trx_code' 		=> 'TRX'.date('ymdhis'),
			'service_code'  => $checking->service_code,
			'service_name'  => $checking->name_service,
			'service_price' => $checking->price
		];

		$dataTransactions = [
			'trx_code'		=> 'TRX'.date('ymdhis'),
			'base_price'    => ((int) $checking->price * $qty),
			'id_user'       => $this->session->userdata('id'),
			'name_user'     => $this->session->userdata('name'),
			'email_user'    => $this->session->userdata('email'),
			'username_user' => $this->session->userdata('username'),
			'telp_user'     => $this->session->userdata('telp'),
			'status'        => 'progress',
			'service_id'    => $checking->id,
			'service_code'  => $checking->service_code,
			'service_name'  => $checking->name_service,
			'remarks'       => 'pemesanan awal',
			'payments'      => $payment,
			'status_payment'=> 'pending',
			'qty'		    => $qty,
		];

		try {

			$this->db->insert('tbl_transactions',$dataTransactions);
			$lastTransaction = $this->db->insert_id();
			
			if(!empty($_FILES['img']))
			{
				$tmp_name = $_FILES["img"]["tmp_name"];
				$name = $_FILES["img"]["name"];
				$path = "assets/transactions/document/checkout/".$name;
				move_uploaded_file($tmp_name, $path);

				$dataDocuments = [
					'trx_id'       => $lastTransaction,
					'trx_code'     => 'TRX'.date('ymdhis'),
					'service_code' => $checking->service_code,
					'service_name' => $checking->name_service,
					'id_user'      => $this->session->userdata('id'),
					'name_user'    => $this->session->userdata('name'),
					'name_file'    => $name,
					'path_file'    => $path,
					'status'       => 'raw_customers'
				];

				$this->db->insert('tbl_transaction_documents',$dataDocuments);
			}


			for ($i=0; $i < $qty; $i++) {
				$data['trx_id'] = $lastTransaction;	
				$this->db->insert('tbl_transaction_services',$data);
			}
			$this->session->set_flashdata('messages','Transaksi pemesanan anda pending. segera lakukan pembayaran agar dapat di proses ke tahap selanjutnya');

		} catch (\Throwable $th) {
			echo json_encode('maaf sedang ada kendala teknis. silahkan coba beberapa saat lagi.');
		}
	}

}
