<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Callback extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		redirect('home');
	}

    public function success()
    {
		$this->template->load('templates/home_template','home/callback/checkout/success');
    }

    public function failed()
    {
		$this->template->load('templates/home_template','home/callback/checkout/failed');
    }

}
