<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		authentication($this);
		$this->load->model("model_services","services");
		$this->load->model("model_service_category","service_category");
	}

	public function index()
	{
		// untuk mengambil data session login
        $data=dashboard_data($this);
        $data['category'] = $this->db->get_where('tbl_service_category',array('deleted'=>0))->result();
		$this->template->load('templates/dashboard_template','dashboard/services/index',$data);
	}

	public function category()
	{
		// untuk mengambil data session login
		$data=dashboard_data($this);
		$this->template->load('templates/dashboard_template','dashboard/service_category/index',$data);
	}

	public function datatables_services()
	{
		$list = $this->services->get_datatables();

		$data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();

            $row['no']                    = $no;
            $row['id']                    = $l->id;
            $row['description']           = $l->description;
            $row['service_category_id']   = $l->service_category_id;
            $row['name_service']          = $l->name_service;
            $row['service_code']          = $l->service_code;
            $row['name_service_category'] = $l->name_service_category;
            $row['price']                 = $l->price;
            $row['images']                = $l->images;
            $row['created_on']            = $l->created_on;
            
            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->services->count_all(),
            "recordsFiltered"   => $this->services->count_filtered(),
            "data"              => $data,
        );

        //output to json format
        echo json_encode($output);
	}

	public function datatables_service_category()
	{
		$list = $this->service_category->get_datatables();

		$data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();

            $row['no']         = $no;
            $row['id']         = $l->id;
            $row['name']       = $l->name;
            $row['created_on'] = $l->created_on;
            
            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->service_category->count_all(),
            "recordsFiltered"   => $this->service_category->count_filtered(),
            "data"              => $data,
        );

        //output to json format
        echo json_encode($output);
    }
    
    public function save_services()
    {
        $exp_service_category = explode('-',$this->input->post('service_category'));
        $category             = str_replace('_',' ',$exp_service_category[1]);
        $id   = $this->input->post('id');
        $data = array(
            'name_service'          => $this->input->post('service_name'),
            'service_code'          => $this->input->post('service_code'),
            'description'           => $this->input->post('description'),
            'name_service_category' => $category,
            'service_category_id'   => $exp_service_category[0],
            'price'                 => $this->input->post('price'),
        );

        
        $nameImages = "slider.png";
        if(empty($id))
        {
            // aksi untuk insert
            try {
                //code...
                $checkImages = $this->db->get_where('tbl_services',['id' => $id])->row();
                if(!empty($_FILES['img']))
                {
                    $tmp_name = $_FILES["img"]["tmp_name"];
                    $name = $_FILES["img"]["name"];
                    $path = "assets/img/service/".$name;
                    move_uploaded_file($tmp_name, $path);
                    
                    $data['images'] = $name;   
                } else{
                    $data['images'] = $nameImages;   
                }

                $this->db->insert('tbl_services',$data);
                $message = array('messages' => 'add services <b>'.$data['name_service'].'</b> success');
                echo json_encode($message);
                exit;
            } catch (\Throwable $th) {
                //throw $th;
                $message = array('messages' => 'add services <b>'.$data['name_service'].'</b> failed');
                echo json_encode($message);
                exit;
            }
        } else {
            // aksi untuk edit
            try {
                //code...

                if(!empty($_FILES['img']))
                {
                    $tmp_name = $_FILES["img"]["tmp_name"];
                    $name = $_FILES["img"]["name"];
                    $path = "assets/img/service/".$name;
                    move_uploaded_file($tmp_name, $path);
                    
                    $data['images'] = $name;   
                }
                
                $this->db->where('id',$id);
                $this->db->update('tbl_services',$data);
                $message = array('messages' => 'edit services <b>'.$data['name_service'].'</b> success');
                echo json_encode($message);
                exit;
            } catch (\Throwable $th) {
                //throw $th;
                $message = array('messages' => 'edit services <b>'.$data['name_service'].'</b> failed');
                echo json_encode($message);
                exit;
            }
        }
    }

    public function save_service_category()
    {
        $id   = $this->input->post('id');
        $name = $this->input->post('name');
        $data = array(
            'name' => $name,
            'code' => str_replace(' ','_',$name)
        );

        if(empty($id))
        {
            // aksi untuk insert
            try {
                //code...
                $this->db->insert('tbl_service_category',$data);
                $message = array('messages' => 'add services category <b>'.$data['name'].'</b> success');
                echo json_encode($message);
                exit;
            } catch (\Throwable $th) {
                //throw $th;
                $message = array('messages' => 'add services category <b>'.$data['name'].'</b> failed');
                echo json_encode($message);
                exit;
            }
        } else {
            // aksi untuk edit
            try {
                //code...
                $this->db->where('id',$id);
                $this->db->update('tbl_service_category',$data);

                $this->db->where('service_category_id',$id);
                $this->db->update('tbl_services',array('name_service_category' => $name));
                
                $message = array('messages' => 'edit services category <b>'.$data['name'].'</b> success');
                echo json_encode($message);
                exit;
            } catch (\Throwable $th) {
                //throw $th;
                $message = array('messages' => 'edit services category <b>'.$data['name'].'</b> failed');
                echo json_encode($message);
                exit;
            }
        }
    }

    public function deleted_services($id)
    {
        $getname = $this->db->get_where('tbl_services',array('id'=>$id))->row();
        try {
            //code soft deleted...
            $this->db->where('id',$id);
            $this->db->update('tbl_services',array('deleted'=>'1'));
            $message = array('messages' => 'data services <b>'.$getname->name_service.'</b> has deleted.');
            echo json_encode($message);
            exit;
        } catch (\Throwable $th) {
            //throw $th;
            $message = array('messages' => 'data services <b>'.$getname->name_service.'</b> failed deleted.');
            echo json_encode($message);
            exit;
        }
    }

    public function deleted_service_category($id)
    {
        $getname = $this->db->get_where('tbl_service_category',array('id'=>$id))->row();
        try {
            //code soft deleted...
            $this->db->where('id',$id);
            $this->db->update('tbl_service_category',array('deleted'=>'1'));
            $message = array('messages' => 'data services category <b>'.$getname->name.'</b> has deleted.');
            echo json_encode($message);
            exit;
        } catch (\Throwable $th) {
            //throw $th;
            $message = array('messages' => 'data services category <b>'.$getname->name.'</b> failed deleted.');
            echo json_encode($message);
            exit;
        }
    }
}
