<!-- Breadcrumb -->
<ol class = "breadcrumb">
        <li><a href = "<?php echo base_url('dashboard'); ?>"><i class = "fa fa-dashboard"></i> Dashboard</a></li>
        <li class = "active">Services</li>
</ol>

<!-- Main Content -->

<section class = "content">

<div style="display:none" class="alert alert-success alert-dismissible" id="notification-success">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i>notifications</h4>
      <div id="messages"></div>
</div>

  <div class = "box">
    <div class = "box-header with-border">
      <h2 class = "box-title"> List Services </h2>
      <button type="button" class="btn bg-olive btn-flat margin" id="add">add</button>
    </div>

  <div class = "box-body">
    <div class = "">
      <table class = "table table-bordered table-hover" id="default-table-services">
        <thead>
          <tr>
            <th>No</th>
            <th>Services Name</th>
            <th>Services Code</th>
            <th>Services Category</th>
            <th>Description</th>
            <th>Price</th>
            <th>Created</th>
            <th>Actions</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>

</div>


<!-- Modal -->
<div class="modal fade" id="modal-service" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-center">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="title-modal"></h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-save" method="POST">

              <!-- .box-body -->
              <div class="row">
                <div class="col-md-8">
                    <div class="box-body">
                        <input name="id" id="id" type="hidden">
                        <div class="form-group">
                          <label for="service-name">Services Name</label>
                          <input onkeypress="allowAlphaNumericSpace(event)" required type="text" class="form-control" name="service_name" id="service-name" placeholder="Services Name">
                        </div>
                        <div class="form-group">
                          <label for="service-code">Services Code</label>
                          <input onkeypress="allowAlphaNumericSpace_(event)" required type="text" class="form-control" name="service_code" id="service-code" placeholder="Services Code">
                        </div>
                        <div class="form-group">
                          <label for="service-code">Description</label>
                          <textarea class="form-control" name="description" id="description" placeholder="Description"></textarea>
                        </div>
                        <div class="form-group">
                          <label for="">Services Category</label>
                          <select required class="form-control" name="service_category" id="service-category">
                          <option value="0">-</option>
                              <?php
                                foreach ($category as $key) {
                                  echo "<option value=".$key->id."-".$key->code.">".$key->name."</option>";
                                }
                              ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="service-price">Prices</label>
                          <input required type="number" class="form-control" name="price" id="service-price" placeholder="Prices">
                        </div>
                  </div>
                </div>
                <div class="col-md-4">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="img">Images</label>
                          <input type="file" class="form-control" name="img" id="img">
                          <img id="img-path" style="width: 268px;">
                      </div>
                    </div>
                </div>
              </div>
              
              <!-- /.box-body -->
                
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

          </form>
        </div>
      </div>
      
    </div>
</div>

<div class="modal fade" id="modal-service-delete" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Services</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-delete">

          <!-- .box-body -->
          <div class="box-body">
                <input name="id" value="" type="hidden">
                <p>Add Services</p>
                
             </div>
              <!-- /.box-body -->

            
            <div class="modal-footer">
              <button type="submit" class="btn btn-danger">Delete</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
      
    </div>
</div>

<!-- end Modal -->

<script type="text/javascript">
    $(document).ready(function() {

      // open modal
      $('#add').on('click', function(e){
          $('#modal-service').modal('show');
          $('#title-modal').html('Add Services');

          $('#id').val(null);
          $('#service-name').val(null);
          $('#service-code').val(null);
          $('#description').val(null);
          $('#service-category').val(null);
          $('#service-price').val(null);
          $('#img').val(null);
          $('#img-path').removeAttr('src');

      });

      $('.form-save').submit(function(e) {
          e.preventDefault();
          $.ajax({
            url: baseurl+'services/save_services',
            type: "POST",
            dataType: 'JSON',
            processData: false,
            contentType: false,
            data: new FormData(this),
            success: function (data) {
              $('#modal-service').modal('hide');
              $('#default-table-services').DataTable().ajax.reload();
              $('#messages').html(data.messages);
              $('#notification-success').show();

            }
          })
      });


      var table_services = $('#default-table-services').DataTable({
            // "scrollX": true,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // "scrollX": true,
            "searching": true,

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('services/datatables_services') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                    "data": "no"
                },
                {
                    "targets": [1],
                    "orderable": true,
                    "data": "name_service"
                },
                {
                    "targets": [2],
                    "orderable": true,
                    "data": "service_code"
                },
                {
                    "targets": [3],
                    "orderable": true,
                    "data": "name_service_category"
                },
                {
                    "targets": [4],
                    "orderable": true,
                    "data": "description"
                },
                {
                    "targets": [5],
                    "orderable": true,
                    "data": "price"
                },
                {
                    "targets": [6],
                    "orderable": true,
                    "data": "created_on"
                },
                {
                    "targets": [7],
                    "orderable": false,
                    "data": {
                        "edit": "edit",
                        "delete": "delete"
                    },
                    "render": function(data, type, row, meta) {
                        return '<button data-idcat='+data.service_category_id+' data-id=' + row.id + ' type="button" class="btn btn-success btn-update"><i class="fa fa-pencil"></i></button> &nbsp;'+ 
                        '<button data-id=' + row.id + ' type="button" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></button>'
                    }
                }
            ]
      });


        // for deleted
        $("#default-table-services tbody").on('click', 'tr .btn-delete', function(e) {
          e.preventDefault();
          $('#modal-service-delete').modal('show');
          
          var id   = $(this).attr('data-id');

          $('.form-delete').submit(function(e) {
            e.preventDefault();

            $.ajax({
              type: 'POST',
              data: { id: id },
              url: baseurl+'services/deleted_services/'+id,
              dataType: 'JSON',
              processData: false,
              contentType: false,
              success: function(data) {
                $('#modal-service-delete').modal('hide');
                $('#default-table-services').DataTable().ajax.reload();
                $('#messages').html(data.messages);
                $('#notification-success').show();
              }
            });
          });
        });

        // for updated
        $('#default-table-services tbody').on('click', 'tr .btn-update', function(e) {
          e.preventDefault();

          $('#modal-service').modal('show');
          $('#title-modal').html('Edit Services');

          var id          = $(this).attr('data-id');
          var id_category = $(this).attr('data-idcat');
          var data        = table_services.row($(this).parents('tr')).data();
          var code        = data.name_service_category.replace(/ /gi,'_');

          $('#id').val(id);
          $('#service-name').val(data.name_service);
          $('#service-code').val(data.service_code);
          $('#description').val(data.description);
          $('#service-category').val(id_category+'-'+code);
          $('#service-price').val(data.price);
          $('#img-path').attr('src','<?=base_url()?>assets/img/service/'+data.images);

          if(data.images == null || data.images == ''){
            $('#img-path').attr('src','<?=base_url()?>assets/img/service/slider.png');
          }

          $('.form-save').submit(function(e) {
            e.preventDefault();

            $.ajax({
              type: 'POST',
              data: new FormData(this),
              url: baseurl+'services/save_services',
              dataType: 'JSON',
              processData: false,
              contentType: false,
              success: function(data) {
                $('#modal-service').modal('hide');
                $('#default-table-services').DataTable().ajax.reload();
                $('#messages').html(data.messages);
                $('#notification-success').show();
              }
            });
          });
        });
});
</script>


