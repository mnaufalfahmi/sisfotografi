
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Tambah User</h3>
    </div>

    <?php
      echo form_open_multipart('user/input','class="form-horizontal"');
      ?>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">NIK</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="nik" placeholder="NIK" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name_user" placeholder="Nama" required>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
            <input required type="text" class="form-control" name="username" placeholder="Username">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
            <input required type="password" class="form-control" name="password" placeholder="Password">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Role</label>
            <div class="col-sm-10">
                <select required name="id_role" class="select2 form-control">
                    <option value="0">Pilih Role</option>
                        <?php
                            foreach ($user_roles as $ur) 
                            {
                                echo "<option value='$ur->id_role'>$ur->role_name</option>";
                            }
                        ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Laboratorium</label>
            <div class="col-sm-10">
                <select name="id_lab" class="select2 form-control" required>
                    <option value="0" required>Pilih laboratorium</option>
                        <?php
                            foreach ($lab as $kd)
                            {                          
                                echo "<option value='$kd->id_lab'>$kd->name_lab</option>";
                            }
                        ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
            <input required type="email" class="form-control" name="email_user" placeholder="Email">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Jenis Kelamin</label>
            <div class="col-sm-10">
            <select name  = "jenis_kelamin" class = "form-control" required>
                <option value = "0">Pilih Jenis Kelamin</option>
                <option value = "Laki-Laki">Laki Laki</option>
                <option value = "Perempuan">Perempuan</option>
            </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Telp</label>
            <div class="col-sm-10">
            <input required type="text" class="form-control" name="telp_user" placeholder="Telp">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Foto</label>
            <div class="col-sm-10">
            <input required type="file" class="form-control" name="userfile" accept="image/*" onchange="loadFile(event)">
            <img id="imagePreview"/>
            </div>
        </div>

    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-info pull-right" name="submit">Tambah User</button>
    </div>
  </form>
</div>
</div>

<style>
#imagePreview {
    margin-top: 7px;
    width: 200px;
    height: auto;
    background-position: center center;
    background-size: cover;
    -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
    display: inline-block;
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
    
}
</style>
<script>
  var loadFile = function(event) {
    var imagePreview = document.getElementById('imagePreview');
    imagePreview.src = URL.createObjectURL(event.target.files[0]);
  };
</script>