<!-- Breadcrumb -->

<ol class = "breadcrumb">

        <li><a href = "<?php echo base_url('dashboard'); ?>"><i class = "fa fa-dashboard"></i> Dashboard</a></li>

        <li class = "active">User</li>

      </ol>

</section>



<!-- Main Content -->

<section class = "content">

  <div class = "box">

    <div class = "box-header with-border">

      <h2 class = "box-title"> Tabel User </h2>
      <small>
        <?php
        echo anchor('user/input','<i class="glyphicon glyphicon-plus"></i>','class="btn btn-xs bg-green"');
        ?>
      </small>

      <br>

      <br>

      

    </div>

  <div class = "box-body">

    <div class = "table-responsive">

      <table class = "table table-bordered table-hover" id = "default-table">

        <thead>

          <tr>

          <th>No</th>
                                    <th>NIK</th>
                                  <th>Nama</th>
                                  <th>Username</th>
                                  <th>Laboratorium</th>
                                  <th>Email</th>
                                  <th>Peran User</th>
                                  <!-- <th>Tanggal</th> -->
                                  <th>Aksi</th>

          </tr>

        </thead>

        <tbody>

          <?php

        $no = 1;

          foreach ($user->result() as $r) {

            echo "<tr>

            <td>$no</td>
            <td>$r->nik</td>
            <td>$r->name_user</td>
            <td>$r->username</td>
            <td>$r->name_lab</td>
            <td>$r->email_user</td>
            <td>$r->role_name</td>



              <td>".anchor('user/edit/'.$r->id_user,'<i class="fa fa-pencil"></i>', array('title' => 'Edit','class' => 'btn bg-purple btn-xs'))." 
               ".anchor('user/delete/'.$r->id_user,'<i class="fa fa-remove"></i>', array('title' => 'Hapus', 'class' => 'btn btn-danger btn-xs', 'onclick'=>"return confirm('Apakah anda yakin ingin menghapus data ini?')"))."</td>



            </tr>";

            $no++;

          }

          ?>

        </tbody>

      </table>

    </div>

  </div>

</div>


