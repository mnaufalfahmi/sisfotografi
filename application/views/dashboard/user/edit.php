
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Edit User</h3>
    </div>

    <?php
      echo form_open_multipart('user/edit','class="form-horizontal"');
      ?>
    <input type="hidden" name="id" value="<?php echo $user['id_user']; ?>">
    <input type="hidden" name="image_user" value="<?php echo $user['image_user']; ?>">
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">NIK</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="nik" placeholder="NIK" required value="<?php echo $user['nik'];?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name_user" placeholder="Nama" required value="<?php echo $user['name_user'];?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
            <input required type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $user['username'];?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
            <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Role</label>
            <div class="col-sm-10">
                <select required name="id_role" class="select2 form-control">
                    <option value="0">Pilih role</option>
                    <?php
                    foreach ($user_roles as $ur) 
                    {
                        echo "<option value='$ur->id_role'";
                        echo $user['id_role']==$ur->id_role?'selected':'';
                        echo ">$ur->role_name</option>";
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Laboratorium</label>
            <div class="col-sm-10">
                <select name="id_lab" class="select2 form-control" required>
                    <option value="0" required>Pilih laboratorium</option>
                        <?php
                        foreach ($lab as $ur) 
                        {
                            echo "<option value='$ur->id_lab'";
                            echo $user['id_lab']==$ur->id_lab?'selected':'';
                            echo ">$ur->name_lab</option>";
                        }
                        ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
            <input required type="email" class="form-control" name="email_user" placeholder="Email" value="<?php echo $user['email_user'];?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Jenis Kelamin</label>
            <div class="col-sm-10">
            <select required="required" class="form-control" name="jenis_kelamin">
                <option value="0">Pilih jenis kelamin</option>
                <option value="laki-laki" <?php if ($user['jenis_kelamin']=="laki-laki") echo 'selected=""'; ?>>Laki-laki</option>
                <option value="perempuan" <?php if ($user['jenis_kelamin']=="perempuan") echo 'selected=""'; ?>>Perempuan</option>
            </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Telp</label>
            <div class="col-sm-10">
            <input required type="text" class="form-control" name="telp_user" placeholder="Telp" value="<?php echo $user['telp_user'];?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Foto</label>
            <div class="col-sm-10">
            <input type="file" class="form-control" name="userfile" accept="image/*" onchange="loadFile(event)">
            <img id="imagePreview" src="<?PHP echo base_url().'assets/img/user/'.$user['image_user'];?>"/>
            </div>
        </div>

    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-info pull-right" name="submit">Edit User</button>
    </div>
  </form>
</div>
</div>

<style>
#imagePreview {
    margin-top: 7px;
    width: 200px;
    height: auto;
    background-position: center center;
    background-size: cover;
    -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
    display: inline-block;
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
    
}
</style>
<script>
  var loadFile = function(event) {
    var imagePreview = document.getElementById('imagePreview');
    imagePreview.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
