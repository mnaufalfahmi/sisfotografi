<!-- Breadcrumb -->
<ol class = "breadcrumb">
        <li><a href = "<?php echo base_url('dashboard'); ?>"><i class = "fa fa-dashboard"></i> Dashboard</a></li>
        <li class = "active">Service Category</li>
</ol>

<!-- Main Content -->

<section class = "content">

<div style="display:none" class="alert alert-success alert-dismissible" id="notification-success">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i>notifications</h4>
      <div id="messages"></div>
</div>

  <div class = "box">
    <div class = "box-header with-border">
      <h2 class = "box-title"> List Service Category </h2>
      <button type="button" class="btn bg-olive btn-flat margin" id="add">add</button>
    </div>

  <div class = "box-body">
    <div class = "">
      <table class = "table table-bordered table-hover" id="default-table-service-category">
        <thead>
          <tr>
            <th>No</th>
            <th>Name Services Category</th>
            <th>Created</th>
            <th>Action</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>

</div>


<!-- Modal -->
<div class="modal fade" id="modal-service-category" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="title-modal"></h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-save">

          <!-- .box-body -->
          <div class="box-body">
              <input name="id" id="id" type="hidden">
                <div class="form-group">
                  <label for="service-name">Services Category Name</label>
                  <input onkeypress="allowAlphaNumericSpace(event)" required type="text" class="form-control" name="name" id="service-name" placeholder="Services Category Name">
                </div>
             </div>
              <!-- /.box-body -->

            
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
      
    </div>
</div>

<div class="modal fade" id="modal-service-category-delete" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Services Category</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-delete">

          <!-- .box-body -->
          <div class="box-body">
                <p>Delete Services Category</p>
                
             </div>
              <!-- /.box-body -->

            
            <div class="modal-footer">
              <button type="submit" class="btn btn-danger">Delete</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
      
    </div>
</div>
<!-- end Modal -->

<script type="text/javascript">
    $(document).ready(function() {

      // open modal
      $('#add').on('click', function(e){
        $('#modal-service-category').modal('show');
        $('#title-modal').html('Add Services Category');
        
        $('#id').val(null);
        $('#service-name').val(null);

      });

      $('.form-save').submit(function(e) {
          e.preventDefault();
          $.ajax({
            url: baseurl+'services/save_service_category',
            type: "POST",
            dataType: 'JSON',
            processData: false,
            contentType: false,
            data: new FormData(this),
            success: function (data) {
              $('#modal-service-category').modal('hide');
              $('#default-table-service-category').DataTable().ajax.reload();
              $('#messages').html(data.messages);
              $('#notification-success').show();

            }
          })
      });

      var table_services_category =  $('#default-table-service-category').DataTable({
            // "scrollX": true,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // "scrollX": true,
            "searching": true,

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('services/datatables_service_category') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                    "data": "no"
                },
                {
                    "targets": [1],
                    "orderable": true,
                    "data": "name"
                },
                {
                    "targets": [2],
                    "orderable": true,
                    "data": "created_on"
                },
                {
                    "targets": [3],
                    "orderable": false,
                    "data": {
                        "edit": "edit",
                        "delete": "delete"
                    },
                    "render": function(data, type, row, meta) {
                        return '<button data-id=' + row.id + ' type="button" class="btn btn-success btn-update"><i class="fa fa-pencil"></i></button> &nbsp;'+ 
                        '<button data-id=' + row.id + ' type="button" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></button>'
                    }
                }
            ]
      });

      // for deleted
      $("#default-table-service-category tbody").on('click', 'tr .btn-delete', function(e) {
          e.preventDefault();
          $('#modal-service-category-delete').modal('show');
          
          var id   = $(this).attr('data-id');

          $('.form-delete').submit(function(e) {
            e.preventDefault();

            $.ajax({
              type: 'POST',
              data: { id: id },
              url: baseurl+'services/deleted_service_category/'+id,
              dataType: 'JSON',
              processData: false,
              contentType: false,
              success: function(data) {
                $('#modal-service-category-delete').modal('hide');
                $('#default-table-service-category').DataTable().ajax.reload();
                $('#messages').html(data.messages);
                $('#notification-success').show();
              }
            });
          });
      });

      // for updated
      $('#default-table-service-category tbody').on('click', 'tr .btn-update', function(e) {
          e.preventDefault();

          $('#modal-service-category').modal('show');
          $('#title-modal').html('Edit Services Category');

          var id   = $(this).attr('data-id');
          var data = table_services_category.row($(this).parents('tr')).data();
          $('#id').val(id);
          $('#service-name').val(data.name);

          $('.form-save').submit(function(e) {
            e.preventDefault();

            $.ajax({
              type: 'POST',
              data: new FormData(this),
              url: baseurl+'services/save_service_category',
              dataType: 'JSON',
              processData: false,
              contentType: false,
              success: function(data) {
                $('#modal-service-category').modal('hide');
                $('#default-table-service-category').DataTable().ajax.reload();
                $('#messages').html(data.messages);
                $('#notification-success').show();
              }
            });
          });
      });

    });
</script>


