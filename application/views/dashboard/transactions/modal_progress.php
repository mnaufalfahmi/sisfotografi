<div class="modal fade" id="modal-transactions-progress" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Progress Pengerjaan</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-save-progress" method="POST">

            <!-- .box-body -->
            <div class="box-body">
                  <input name="id" id="id-progress" type="hidden">
                  
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="workmanship-status">Progress Pengerjaan</label>
                      <select required class="form-control" name="workmanship_status" id="workmanship-status">
                        <option value="0">-</option>
                        <option value="progress">progress</option>
                        <option value="success">success</option>
                        <option value="failed">failed</option>
                        <option value="canceled">canceled</option>
                      </select>
                    </div>
                  </div>
                  
                  <br>
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="id">Files to customers</label>
                      <input class="form-control" type="file" name="fileProgress" id="file">
                      <!-- <a id="evidence_transfer_progress" target="_blank"></a> -->
                    </div>
                  </div>

                  <br>
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="remarks">Messages</label>
                      <textarea class="form-control" name="remarks" id="remarks-progress"></textarea>
                    </div>
                  </div>
                  <br>

                  </div>

                  <br>
            </div>
            <!-- /.box-body -->
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>

          </form>
        </div>
      </div>
      
    </div>
</div>