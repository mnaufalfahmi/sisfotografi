<div class="modal fade" id="modal-transactions" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit transactions</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-save" method="POST">

            <!-- .box-body -->
            <div class="box-body">
                  <input name="id" id="id" type="hidden">
                  
                  <div class="row">
                    <div class="col-sm-6">
                        <label for="trx-code">Trx Code</label>
                        <input disabled="" class="form-control" name="trx_code" id="trx-code">
                    </div>
                    <div class="col-lg-6">
                        <label for="price">Price</label>
                        <input disabled="" class="form-control" name="price" id="price">
                    </div>
                  </div>

                  <br>
                  <div class="row">
                    <div class="col-sm-12">
                        <label for="service-code">Type Payment</label>
                          <select disabled required class="form-control" name="type_payment" id="type-payment">
                            <option value="0">-</option>
                            <option value="on_location">On Location</option>
                            <option value="online">online</option>
                          </select>
                    </div>
                  </div>

                  <br>
                  <div class="row">
                    <div class="col-sm-8">
                      <label for="service-code">Status Payment</label>
                        <select required class="form-control" name="status_payment" id="status-payment">
                          <option value="0">-</option>
                          <option value="pending">pending</option>
                          <option value="settlement">settlement</option>
                          <option value="cancel">cancel</option>
                          <option value="failed">failed</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                      <label>Evidence Transfer</label>
                      <input class="form-control" type="file" name="file" id="file-evidence">
                      <a id="evidence_transfer" target="_blank"></a>
                    </div>
                  </div>

                  <br>
                  <div class="row">
                  <div class="col-sm-12">
                      <label for="service-code">Status Evidence Transfer</label>
                        <select required class="form-control" name="status_evidence_transfer" id="status-evidence">
                          <option value="0">-</option>
                          <option value="accepted">Accepted</option>
                          <option value="rejected">Rejected</option>
                          <option value="waiting">Waiting Admin</option>
                          <option value="none">None</option>
                        </select>
                    </div>
                  </div>
                  
                  <br>
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="remarks">Messages</label>
                      <textarea class="form-control" name="remarks" id="remarks"></textarea>
                    </div>
                  </div>
                  <br>

            </div>
            <!-- /.box-body -->
              
              <div class="modal-footer">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>

          </form>
        </div>
      </div>
      
    </div>
</div>