<!-- Breadcrumb -->
<ol class = "breadcrumb">
        <li><a href = "<?php echo base_url('dashboard'); ?>"><i class = "fa fa-dashboard"></i> Dashboard</a></li>
        <li class = "active">Transactions</li>
</ol>

<!-- Main Content -->

<section class = "content">
  <div class = "box">
    <div class = "box-header with-border">
      <h2 class = "box-title"> List transactions </h2>
    </div>

  <div class = "box-body">
    <div class = "">
      <table class = "table table-bordered table-hover" id="default-table-transactions">
        <thead>
          <tr>
            <th>No</th>
            <th>Action</th>
            <th>Trx code</th>
            <th>Workmanship Status</th>
            <th>Status Payment</th>
            <th>price</th>
            <th>Name User</th>
            <th>Telp User</th>
            <th>Service code</th>
            <th>Service Name</th>
            <th>Type Payment</th>
            <th>Remarks</th>
            <th>Created</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>

</div>


<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#default-table-transactions').DataTable({
            // "scrollX": true,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // "scrollX": true,
            "searching": true,

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('transactions/datatables') ?>",
                "type": "POST"
            },
            "columnDefs": [{
              "targets": [0],
                "data": "no",
                  render: function(data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                      console.log(data)
                  }
                },
                {
                    "targets": [1],
                    "orderable": true,
                    "data": "action"
                },
                {
                    "targets": [2],
                    "orderable": true,
                    "data": "trx_code"
                },
                {
                    "targets": [3],
                    "orderable": true,
                    "data": "status"
                },
                {
                    "targets": [4],
                    "orderable": true,
                    "data": "status_payment"
                },
                {
                    "targets": [5],
                    "orderable": true,
                    "data": "base_price"
                },
                {
                    "targets": [6],
                    "orderable": true,
                    "data": "name_user"
                },
                {
                    "targets": [7],
                    "orderable": true,
                    "data": "telp_user"
                },
                {
                    "targets": [8],
                    "orderable": true,
                    "data": "service_code"
                },
                {
                    "targets": [9],
                    "orderable": true,
                    "data": "service_name"
                },
                {
                    "targets": [10],
                    "orderable": true,
                    "data": "payments"
                },
                {
                    "targets": [11],
                    "orderable": true,
                    "data": "remarks"
                },
                {
                    "targets": [12],
                    "orderable": true,
                    "data": "created_on"
                }
            ]
        });

        table.on('order.dt search.dt', function() {
          table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
              cell.innerHTML = i + 1;
          });
        }).draw();


    });
</script>


