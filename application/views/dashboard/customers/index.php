<!-- Breadcrumb -->
<ol class = "breadcrumb">
        <li><a href = "<?php echo base_url('dashboard'); ?>"><i class = "fa fa-dashboard"></i> Dashboard</a></li>
        <li class = "active">Customers</li>
</ol>

<!-- Main Content -->

<section class = "content">
  <div class = "box">
    <div class = "box-header with-border">
      <h2 class = "box-title"> List Customers </h2>
    </div>

  <div class = "box-body">
    <div class = "">
      <table class = "table table-bordered table-hover" id="default-table-customers">
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Phone</th>
            <th>Status</th>
            <th>Created</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>

</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#default-table-customers').DataTable({
            // "scrollX": true,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // "scrollX": true,
            "searching": true,

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('customers/datatables') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                    "data": "no"
                },
                {
                    "targets": [1],
                    "orderable": true,
                    "data": "name"
                },
                {
                    "targets": [2],
                    "orderable": true,
                    "data": "email"
                },
                {
                    "targets": [3],
                    "orderable": true,
                    "data": "gender"
                },
                {
                    "targets": [4],
                    "orderable": true,
                    "data": "telp"
                },
                {
                    "targets": [5],
                    "orderable": true,
                    "data": "status"
                },
                {
                    "targets": [6],
                    "orderable": true,
                    "data": "created_on"
                }
            ]
        });
    });
</script>


