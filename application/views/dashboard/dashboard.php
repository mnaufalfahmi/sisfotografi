<ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
      </ol>
</section>

<!-- Main Content -->
<section class="content">
  <div class="row">
    <div class="col-lg-3 col-xs-3">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $counting['progress']; ?></h3>
                <p>Transactions Progress</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-people"></i>
            </div>
                <!-- <a href="<?php echo base_url('user'); ?>" class="small-box-footer">Info Detail <i class="fa fa-arrow-circle-right"></i></a> -->
        </div>
    </div>

    <div class="col-lg-3 col-xs-3">
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?php echo $counting['success']; ?></h3>
                <p>Transactions Success</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-people"></i>
            </div>
                <!-- <a href="<?php echo base_url('user'); ?>" class="small-box-footer">Info Detail <i class="fa fa-arrow-circle-right"></i></a> -->
        </div>
    </div>

    <div class="col-lg-3 col-xs-3">
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?php echo $counting['failed']; ?></h3>
                <p>Transactions Failed</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-people"></i>
            </div>
                <!-- <a href="<?php echo base_url('user'); ?>" class="small-box-footer">Info Detail <i class="fa fa-arrow-circle-right"></i></a> -->
        </div>
    </div>

    <div class="col-lg-3 col-xs-3">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?php echo $counting['canceled']; ?></h3>
                <p>Transactions canceled</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-people"></i>
            </div>
                <!-- <a href="<?php echo base_url('user'); ?>" class="small-box-footer">Info Detail <i class="fa fa-arrow-circle-right"></i></a> -->
        </div>
    </div>


  </div>
</section>


   