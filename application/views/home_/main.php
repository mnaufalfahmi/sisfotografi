<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<!-- <div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="<?=base_url()?>assets/home_template/Eshopper/#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Sportswear
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Nike </a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Under Armour </a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Adidas </a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Puma</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">ASICS </a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="<?=base_url()?>assets/home_template/Eshopper/#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Mens
										</a>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Fendi</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Guess</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Valentino</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Dior</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Versace</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Armani</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Prada</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Dolce and Gabbana</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Chanel</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Gucci</a></li>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="<?=base_url()?>assets/home_template/Eshopper/#womens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Womens
										</a>
									</h4>
								</div>
								<div id="womens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Fendi</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Guess</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Valentino</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Dior</a></li>
											<li><a href="<?=base_url()?>assets/home_template/Eshopper/#">Versace</a></li>
										</ul>
									</div>
								</div>
							</div> -->

							<?php foreach ($category_services as $key) {
								$isActive = "";
								if($this->uri->segment(3) == $key->code){
									$isActive = "style='color:red;font-size:23px'";
									// echo $this->uri->segment(4);
									// exit;
								}?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title"><a <?=$isActive?> href="<?=base_url()?>home/category/<?=$key->code?>"><?=$key->name?></a></h4>
									</div>
								</div>
							<?php } ?>
						</div><!--/category-products-->
					
						
					
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						<?php foreach ($services as $key) {?>
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
											<div class="productinfo text-center">
												<img src="<?= (empty($key->images) ? base_url('assets/img/service/slider.png') : base_url('assets/img/service/'.$key->images)) ?>" alt="" />
												<h2>Rp <?=number_format($key->price)?></h2>
												<p><?=$key->name_service?></p>
												<a href="<?=base_url()?>home/cart/<?=$key->service_code?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2>Rp <?=number_format($key->price)?></h2>
													<p><?=(empty($key->description) ? $key->name_service : $key->description )?></p>
													<a href="<?=base_url()?>home/cart/<?=$key->service_code?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
									</div>
									<div class="choose">
										<ul class="nav nav-pills nav-justified">
											<li><a href="<?=base_url()?>home/category/<?= str_replace(' ' ,'_', $key->name_service_category)?>"><i class="fa fa-tag"></i><?=$key->name_service_category?></a></li>
										</ul>
									</div>
								</div>
							</div>
						<?php } ?>
						
					</div><!--features_items-->					
					
				</div>

			</div>
		</div>
	</section>
	