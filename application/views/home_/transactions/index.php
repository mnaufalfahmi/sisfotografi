<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/cdn/jquery.dataTables.min.css">

<section>
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 padding-right">
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
				    <h2>Transactions</h2>
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#pending" data-toggle="tab">Transactions Pending</a></li>
								<li><a href="#failed" data-toggle="tab">Transactions Failed</a></li>
								<li><a href="#success" data-toggle="tab">Transactions Success</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="pending">
                                <table class="table" id="table-pending" class="table nowrap w-100" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center"> Action</th>
                                        <th class="text-center"> Evidence Transfer</th>
                                        <th class="text-center"> No TRX</th>
                                        <th class="text-center"> Qty</th>
                                        <th class="text-center"> Total Price</th>
                                        <th class="text-center"> Product</th>
                                        <th class="text-center"> Type Payment</th>
                                        <th class="text-center"> Status Payment</th>
                                        <th class="text-center"> Remarks</th>
                                        <th class="text-center"> Created</th>
                                    </tr>
                                    </thead>
                                    <tbody> <?php foreach ($transactions as $rows) :
                                        if($rows->status_payment == 'pending'){
                                            if(($rows->status_evidence_transfer) == 'none'){
                                                $action = '<button type="button" style="margin-top: 0px;" class="btn btn-primary trx-id" data-toggle="modal" data-target="#modalDetail"><i class="fa fa-upload"></i> Upload</button>';
                                                $statusTransfer = $rows->status_evidence_transfer;
                                            } elseif(($rows->status_evidence_transfer) == 'rejected'){
                                                $action = '<button type="button" style="margin-top: 0px;" class="btn btn-primary trx-id" data-toggle="modal" data-target="#modalDetail"><i class="fa fa-upload"></i> Upload</button>';
                                                $statusTransfer = $rows->status_evidence_transfer;
                                            } elseif(($rows->status_evidence_transfer) == 'waiting'){
                                                $action = '-';
                                                $statusTransfer = 'On checking';
                                            } else{
                                                $action = '-';
                                                $statusTransfer = $rows->status_evidence_transfer;
                                            }?>
                                        <tr>
                                            <td class="text-center"><?php echo $action; ?></td>
                                            <td class="text-center"> <?php echo $statusTransfer; ?> </td>
                                            <td class="text-center"> <?php echo $rows->trx_code; ?> </td>
                                            <td class="text-center"> <?php echo $rows->qty; ?> </td>
                                            <td class="text-center"> <?php echo 'Rp. '.number_format($rows->base_price); ?> </td>
                                            <td class="text-center"> <?php echo $rows->service_name; ?> </td>
                                            <td class="text-center"> <?=(($rows->payments == 'on_location') ? 'Di lokasi' : $rows->payments); ?> </td>
                                            <td class="text-center"> <?php echo $rows->status_payment; ?> </td>
                                            <td class="text-center"> <?php echo $rows->remarks; ?> </td>
                                            <td class="text-center"> <?php echo $rows->created_on; ?> </td>
                                        </tr>
                                        <?php } endforeach; ?>
                                    </tbody>
                                </table>
							</div>
							
							<div class="tab-pane fade" id="failed">
                                <table class="table" id="table-failed" class="table nowrap w-100" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center"> No TRX</th>
                                        <th class="text-center"> Qty</th>
                                        <th class="text-center"> Total Price</th>
                                        <th class="text-center"> Product</th>
                                        <th class="text-center"> Type Payment</th>
                                        <th class="text-center"> Status Payment</th>
                                        <th class="text-center"> Remarks</th>
                                        <th class="text-center"> Created</th>
                                    </tr>
                                    </thead>
                                    <tbody> <?php foreach ($transactions as $rows) :
                                        if($rows->status_payment == 'failed'){?>
                                        <tr>
                                            <td class="text-center"> <?php echo $rows->trx_code; ?> </td>
                                            <td class="text-center"> <?php echo $rows->qty; ?> </td>
                                            <td class="text-center"> <?php echo 'Rp. '.number_format($rows->base_price); ?> </td>
                                            <td class="text-center"> <?php echo $rows->service_name; ?> </td>
                                            <td class="text-center"> <?=(($rows->payments == 'on_location') ? 'Di lokasi' : $rows->payments); ?> </td>
                                            <td class="text-center"> <?php echo $rows->status_payment; ?> </td>
                                            <td class="text-center"> <?php echo $rows->remarks; ?> </td>
                                            <td class="text-center"> <?php echo $rows->created_on; ?> </td>
                                        </tr>
                                        <?php } endforeach; ?>
                                    </tbody>
                                </table>
							</div>

							<div class="tab-pane fade" id="success">
                                <table class="table" id="table-success" class="table nowrap w-100" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center"> No TRX</th>
                                        <th class="text-center"> Qty</th>
                                        <th class="text-center"> Total Price</th>
                                        <th class="text-center"> Product</th>
                                        <th class="text-center"> Type Payment</th>
                                        <th class="text-center"> Status Payment</th>
                                        <th class="text-center"> Remarks</th>
                                        <th class="text-center"> Created</th>
                                    </tr>
                                    </thead>
                                    <tbody> <?php foreach ($transactions as $rows) :
                                        if($rows->status_payment == 'success'){?>
                                        <tr>
                                            <td class="text-center"> <?php echo $rows->trx_code; ?> </td>
                                            <td class="text-center"> <?php echo $rows->qty; ?> </td>
                                            <td class="text-center"> <?php echo 'Rp. '.number_format($rows->base_price); ?> </td>
                                            <td class="text-center"> <?php echo $rows->service_name; ?> </td>
                                            <td class="text-center"> <?=(($rows->payments == 'on_location') ? 'Di lokasi' : $rows->payments); ?> </td>
                                            <td class="text-center"> <?php echo $rows->status_payment; ?> </td>
                                            <td class="text-center"> <?php echo $rows->remarks; ?> </td>
                                            <td class="text-center"> <?php echo $rows->created_on; ?> </td>
                                        </tr>
                                        <?php } endforeach; ?>
                                    </tbody>
                                </table>
							</div>
						</div>
					</div><!--/category-tab-->
					
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">Progress Pengerjaan</h2>
                            <table class="table" id="table-progress" class="table nowrap w-100" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center"> Action</th>
                                        <th class="text-center"> No TRX</th>
                                        <th class="text-center"> Product</th>
                                        <th class="text-center"> Progress</th>
                                        <th class="text-center"> Type Payment</th>
                                        <th class="text-center"> Status Payment</th>
                                        <th class="text-center"> Remarks</th>
                                        <th class="text-center"> Created</th>
                                    </tr>
                                    </thead>
                                    <tbody> <?php foreach ($transactions_progress as $rows) :
                                        if($rows->status_payment == 'settlement'){
                                            if($rows->status == 'progress'){
                                                $value = '50';
                                                $action = '-';
                                            } elseif($rows->status == 'success'){
                                                $check = $this->db->get_where('tbl_transaction_documents',['trx_code' => $rows->trx_code, 'status' => 'finished'])->row();
                                                if(!empty($check)){
                                                    $action = '<a target="_blank" href="'.base_url(@$check->path_file).'"><button class="btn"><i class="fa fa-download"></i> Download</button></a>';
                                                } else {
                                                    $action = '-';
                                                }
                                                $value = '100';
                                            } ?>
                                        <tr>
                                            <td class="text-center"><?=@$action?></td>
                                            <td class="text-center"> <?php echo $rows->trx_code; ?> </td>
                                            <td class="text-center"> <?php echo $rows->service_name; ?> </td>
                                            <td class="text-center"> <?php echo $rows->status; ?><br>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: <?=@$value?>%;" aria-valuenow="<?=@$value?>" aria-valuemin="0" aria-valuemax="100"><?=@$value?>%</div>
                                            </div> </td>
                                            <td class="text-center"> <?=(($rows->payments == 'on_location') ? 'Di lokasi' : $rows->payments); ?> </td>
                                            <td class="text-center"> <?php echo $rows->status_payment; ?> </td>
                                            <td class="text-center"> <?php echo $rows->remarks; ?> </td>
                                            <td class="text-center"> <?php echo $rows->created_on; ?> </td>
                                        </tr>
                                        <?php } endforeach; ?>
                                    </tbody>
                                </table>
                        <br>
                        <br>
                        <br>
					</div><!--/recommended_items-->
					
				</div>
			</div>
		</div>
	</section>

    <?php $this->load->view('home_/transactions/detail');?>
    
    <script src="<?=base_url()?>assets/home_template/Eshopper/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/cdn/jquery.dataTables.min.js"></script>

    <script>
        pending = $('#table-pending').DataTable({
            responsive:true,
            searching: true,
            pageLength: 25,
            "bLengthChange": true,
            "ordering": true,
            rowReorder: {
                    selector: 'td:nth-child(5)'
                },
        });

        $('#table-pending tbody').on('click', '.trx-id', function() {
            var data = pending.row($(this).parents('tr')).data();
            $('#code-trx').val(data[2])
        })

        failed = $('#table-failed').DataTable({
            responsive:true,
            searching: true,
            pageLength: 25,
            "bLengthChange": true,
            "ordering": true,
            rowReorder: {
                    selector: 'td:nth-child(5)'
                },
        });

        success = $('#table-success').DataTable({
            responsive:true,
            searching: true,
            pageLength: 25,
            "bLengthChange": true,
            "ordering": true,
            rowReorder: {
                    selector: 'td:nth-child(5)'
                },
        });

        progress = $('#table-progress').DataTable({
            responsive:true,
            searching: true,
            pageLength: 25,
            "bLengthChange": true,
            "ordering": true,
            rowReorder: {
                    selector: 'td:nth-child(5)'
                },
        });
    </script>