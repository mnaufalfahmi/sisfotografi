<!-- Modal -->
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Upload Bukti Pembayaran</h5>
        <button type="button"  style="margin-top: -20px !important" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?=base_url()?>transactions/submitDocuments" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-group">
            <input type="hidden" name="trx_code" id="code-trx">
            <label for="recipient-name" class="col-form-label">Upload Bukti Pembayaran:</label>
            <input type="file" name="upload_files" class="form-control">
          </div>
          <!-- <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" style="margin-top: 0px;" class="btn btn-primary">Submit</button>
      </div>
      </form>


    </div>
  </div>
</div>