<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<?php $no=0; foreach ($services as $num => $key) {?>
							<?php if($key->on_slider == 'yes'){ ?>
								<li data-target="#slider-carousel" data-slide-to="<?=$num?>" <?=(($no++ == 0) ? "class='active'" : "" )?>></li>

							<?php } } ?>
						</ol>
						
						<div class="carousel-inner">
							<?php $no=0; foreach ($services as $num => $key) {?>
							<?php if($key->on_slider == 'yes'){ ?>
							<div class="item <?=(($no++ == 0) ? "active" : "" )?>">
								<div class="col-sm-6">
									<h2><?=$key->name_service?></h2>
									<p><?=$key->description?></p>
									<a href="<?=base_url()?>home/cart/<?=$key->service_code?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
								</div>
								<div class="col-sm-6">
									<img src="<?= (empty($key->images) ? base_url('assets/img/service/slider.png') : base_url('assets/img/service/'.$key->images)) ?>" class="girl img-responsive" alt="" />
								</div>
							</div>
							<?php } } ?>
							
						</div>
						
						<a href="<?=base_url()?>assets/home_template/Eshopper/#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="<?=base_url()?>assets/home_template/Eshopper/#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->