<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-pQt-nuZxvYKXdtve"></script>

<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<form id="myFormSave">
			<input type="hidden" name="paket" value="<?=$this->uri->segment(3)?>">
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
                    <ul class="user_info">
							<li class="single_field zip-field">
								<label>Qty:</label>
                                <input id="qty" name="qty" placeholder="Jumlah Paket" required="" type="number" style="border: 1px solid;" value="1">
								
							</li>
							<li class="single_field">
								<label>Metode Pembayaran:</label>
								<select id="service-paket" name="payment" required>
                                    <option selected value="">Pilih Metode</option>
                                    <option value="on_location">Di lokasi</option>
                                    <option value="online">Online</option>
								</select>
							</li>
                            <li class="single_field zip-field">
								<label>Upload Files:</label>
                                <input required name="img" type="file">
							</li>
						</ul>
						<!-- <a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a> -->
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
                        <li>Price <span>Rp. <?=$services->price?></span></li>
							<li>Qty <span id="number-qty">1</span></li>
							<li>Total <span id="price">Rp. <?=$services->price?></span></li>
						</ul>
							<button class="btn btn-default update" type="submit" >Check Out</button>
					</div>
				</div>
			</form>
			</div>
		</div>
	</section><!--/#do_action-->

    <script src="<?=base_url()?>assets/home_template/Eshopper/js/jquery.js"></script>
	
	<script>
		$(document).ready(function() {
			var session = "<?=$this->session->userdata('user_logged_in')?>";

					$('#qty').on('keyup',function(){	
						// $('#number-qty').html($('#qty').val())
						var data = "<?=$services->price?>";
						if($('#qty').val() == 0 || $('#qty').val() ==  1 || $('#qty').val() == null)
						{
							$('#price').html('Rp. '+data)
							$('#input-price').val(data);
							$('#number-qty').html(1)

						} else{
							var price = $('#qty').val();
							var total = price * data;
							$('#price').html('Rp. '+total)
							$('#input-price').val(total);
							$('#number-qty').html(price)

						}
					})

					$('#myFormSave').submit(function(e) {
						e.preventDefault();
						
						if(session == false)
						{
							alert('silahkan login terlebih dahulu sebelum melakukan pemesanan')
							window.location = "<?=base_url()?>login";
						} else {

							if($('#service-paket').val() == 'on_location'){					
								base_url = "<?=site_url()?>home/submit";
								submitOffline(base_url,new FormData(this));
							} else if($('#service-paket').val() == 'online'){
								base_url = "<?=site_url()?>home/checkoutOnline";
								submitOnline(base_url,new FormData(this));
							} else {
								alert('silahkan pilih metode pembayaran terlebih dahulu')
							}
						}

						
					})
					
		})
	</script>

	<script>
		function submitOnline(urlSubmit,formData)
		{
			$.ajax({
				type: 'POST',
				data: formData,
				url: urlSubmit,
				// dataType: 'JSON',
				processData: false,
				contentType: false,
				cache: false,
				async: true,
				success: function(data, textStatus, xhr) {
			
					if($('#service-paket').val() == 'online') {
								
						// for payment method midtrans
						snap.pay(data, {
							onSuccess: function(result){
								<?php //$this->session->set_flashdata('messages','Transaksi pemesanan anda sukses.');?>
								window.location = "<?=base_url()?>transactions";
								// alert('payment success')			
							},
							onPending: function(result){
								<?php //$this->session->set_flashdata('messages','Transaksi pemesanan anda pending. segera lakukan pembayaran agar dapat di segera di proses ke tahap selanjutnya');?>
								window.location = "<?=base_url()?>transactions";
								// alert('payment pending')
							},
							onError: function(result){
								alert('payment failed')
							}
						});
										
					} else {
						window.location = "<?=base_url()?>transactions";
						// alert('success on location')
					}
				},
				error: function (request, status, error) {
					var obj = JSON.parse(request.responseText);
					alert(obj.error_message);
				}
			})
		}

		function submitOffline(urlSubmit,formData)
		{
			$.ajax({
				type: 'POST',
				data: formData,
				url: urlSubmit,
				// dataType: 'JSON',
				processData: false,
				contentType: false,
				cache: false,
				async: true,
				success: function(data, textStatus, xhr) {
			
					if($('#service-paket').val() == 'online') {
						window.location = "<?=base_url()?>transactions";
										
					} else {
						window.location = "<?=base_url()?>transactions";
					}
				},
				error: function (request, status, error) {
					var obj = JSON.parse(request.responseText);
					alert(obj.error_message);
				}
			})
		}
	</script>