<section class="banner-area relative about-banner" id="home">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
					Login				
				</h1>	
				<p class="text-white link-nav"><a href="<?= base_url() ?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?= base_url() ?>login/customers"> Login</a></p>
			</div>	
		</div>
	</div>
</section>

<section class="appointment-area">			
	<div class="container">
		<div class="row justify-content-between align-items-center pb-120 appointment-wrap">
			<div class="col-lg-12 col-md-12 appointment-right pt-60 pb-60 go-login-center">
				<form class="form-wrap" action="<?= base_url() ?>login/verification" method="POST">
                
                        <input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mb-20 form-control" required="" type="email">

                        <input name="password" placeholder="Enter password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter password'" class="common-input mb-20 form-control" required="" type="password">
					<button class="primary-btn text-uppercase" type="submit" >Login</button>
				</form>
			</div>
		</div>
	</div>	
</section>