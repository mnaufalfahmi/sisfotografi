<?php 

if($this->session->userdata('user_logged_in') == true) {
    $this->load->view('home/dashboard');
} else {
    $this->load->view('home/registration');
}

?>
