<script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="SB-Mid-client-pQt-nuZxvYKXdtve"></script>
    
<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
							<?=$services_category->name?>			
							</h1>	
							<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="#"><?=$services_category->name?></a></p>
						</div>	
					</div>
				</div>
			</section>


<section class="contact-page-area section-gap">
				<div class="container">
					<div class="row">
						
						
						<div class="col-lg-12">
							<form class="" id="myFormSave">
							<input type="hidden" name="url_payment_val" id="url-payment-val" value="<?=base_url()?>home/submit">

								<div class="row">	
									<div class="col-lg-6 form-group">
                                        <div class="form-select services-photo" id="service-select" style="border: 1px solid #ced4da;">
                                            <select name="paket" required>
                                                <option data-display="">--Pilih Paket--</option>
                                                <?php foreach ($services as $key) { ?>
                                                <option data-description="<?=$key->description?>" data-price="<?=$key->price?>" value="<?=$key->id?>"><?=$key->name_service?></option>
                                                 <?php } ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="form-select" id="service-paket-select" style="border: 1px solid #ced4da;">
                                            <select id="service-paket" name="payment" required>
                                                <option data-display="">--Pilih Metode Pembayaran--</option>
                                                <option value="on_location">Di lokasi</option>
                                                <option value="online">Online</option>
                                            </select>
                                        </div>
										<br>    
                                        <input id="qty" name="qty" placeholder="Jumlah Paket" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Jumlah Paket'" class="common-input mb-20 form-control" required="" type="number">
                                        <h5 style="float: left;">Upload File</h5><br>
                                        
                                        <input name="img" class="common-input mb-20 form-control" type="file">
									
										<br>
                                        <br>
                                        <h3 id="price" style="float:left">Harga : Rp. -</h3>
										<input type="hidden" id="input-price" name="price">
									</div>
                                    
                                    <div class="col-lg-4 d-flex flex-column address-wrap">
                                        <div class="single-contact-address d-flex flex-row">
                                            
                                            <div class="contact-details">
                                                <h5 style="float: left;">Keterangan</h5><br>
                                                <p id="description" style="float: left;text-align: justify;white-space: break-spaces;max-width: 350px !important;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            </div>
                                        </div>													
                                    </div>
									<div class="col-lg-12">
										<div class="alert-msg" style="text-align: left;"></div>
										<button id="submit" type="submit" class="genric-btn primary" style="float: right;">Pesan</button>											
									</div>
								</div>
							</form>	
						</div>
					</div>
				</div>	
			</section>

