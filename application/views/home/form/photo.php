<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Cetak Photo				
							</h1>	
							<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="contact.html">Cetak Photo</a></p>
						</div>	
					</div>
				</div>
			</section>


<section class="contact-page-area section-gap">
				<div class="container">
					<div class="row">
						
						
						<div class="col-lg-12">
							<form class="" id="myFormSave">
								<div class="row">	
									<div class="col-lg-6 form-group">
                                        <div class="form-select services-photo" id="service-select" style="border: 1px solid #ced4da;">
                                            <select name="paket" required>
                                                <option data-display="">--Pilih Paket--</option>
                                                <?php foreach ($services as $key) {
                                                    if($key->service_category_id == 4){ ?>
                                                <option data-price="<?=$key->price?>" value="<?=$key->id?>"><?=$key->name_service?></option>
                                                 <?php } } ?>
                                            </select>
                                        </div>
                                        <br>
                                        <input id="qty" name="qty" placeholder="Jumlah Paket" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Jumlah Paket'" class="common-input mb-20 form-control" required="" type="number">
                                        <input name="img" class="common-input mb-20 form-control" type="file">
									
										<br>
                                        <br>
                                        <h3 id="price" style="float:left">Harga : Rp. -</h3>
									</div>
                                    <div class="col-lg-4 d-flex flex-column address-wrap">
                                        <div class="single-contact-address d-flex flex-row">
                                            
                                            <div class="contact-details">
                                                <h5 style="float: left;">Keterangan</h5><br>
                                                <p style="float: left; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            </div>
                                        </div>													
                                    </div>
									<div class="col-lg-12">
										<div class="alert-msg" style="text-align: left;"></div>
										<button id="submit" type="submit" class="genric-btn primary" style="float: right;">Pesan</button>											
									</div>
								</div>
							</form>	
						</div>
					</div>
				</div>	
			</section>

