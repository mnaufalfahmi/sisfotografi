<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Transactions			
							</h1>	
							<p class="text-white link-nav"><a href="<?=base_url()?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?=base_url()?>transactions">Transactions</a></p>
						</div>	
					</div>
				</div>
			</section>

            <div class="whole-wrap">
                <div style="padding: 40px;">
                <div class="section-top-border">
				<?php
				if(!empty($this->session->flashdata('messages'))){ ?>
					<h3 class="mb-30">Info</h3>
						<div class="row">
							<div class="col-lg-12">
								<blockquote class="generic-blockquote"><?=$this->session->flashdata('messages')?></blockquote>
							</div>
						</div><br>
				<?php } ?>
						
						<h3 class="mb-30">List Transaction</h3>
						<div class="progress-table-wrap">
							<div class="progress-table">
								<div class="table-head">
									<div class="serial">#</div>
									<div class="country">Aksi</div>
									<div class="country">TRX CODE</div>
									<div class="country">Paket</div>
									<div class="visit">Total Price</div>
									<div class="percentage">Status Pembayaran</div>
									<div class="percentage">Metode Pembayaran</div>
									<div class="percentage">Status Pengerjaan</div>
									<div class="percentage">Progress</div>
								</div>
                                
                                <?php $no=1; foreach ($transactions as $key) {
                                
                                ?>

                                <div class="table-row">
									<div class="serial"><?=$no?></div>
									<div class="country"><a href="<?=base_url()?>transactions/details/<?=$key->trx_code?>" class="genric-btn primary circle arrow small">detail</a></div>
									<div class="country"><?=$key->trx_code?></div>
									<div class="country"><a href="#"><?=$key->service_name?></a></div>
									<div class="visit">Rp. <?=$key->base_price?></div>
									<div class="percentage"><?=$key->status_payment?></div>
									<div class="percentage"><?=(($key->payments == 'online') ? 'online' : 'di lokasi' )?></div>
									<div class="percentage"><?=$key->status?></div>
									<div class="percentage">
										<div class="progress">
											<div class="progress-bar color-1" role="progressbar" style="width: 20%;height: 16px;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>
										</div>
									</div>
								</div>
                                <?php $no++; } ?>

							</div>
						</div>
					</div>
                </div>
            </div>
                    