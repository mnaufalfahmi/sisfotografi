<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Transactions Details			
							</h1>	
							<p class="text-white link-nav"><a href="<?=base_url()?>">Home </a>  <span class="lnr lnr-arrow-right"></span><a href="<?=base_url()?>transactions">Transactions </a> <span class="lnr lnr-arrow-right"></span> Transactions Details</p>
						</div>	
					</div>
				</div>
			</section>

            <div class="whole-wrap">
                <div style="padding: 40px;">
                <div class="section-top-border">
				<?php
				if(!empty($this->session->flashdata('messages'))){ ?>
					<h3 class="mb-30">Info</h3>
						<div class="row">
							<div class="col-lg-12">
								<blockquote class="generic-blockquote"><?=$this->session->flashdata('messages')?></blockquote>
							</div>
						</div><br>
				<?php } ?>
						
						<h3 class="mb-30">List Your Document</h3>
						<form action="<?=base_url()?>home/submitDocuments" method="POST">
						<div class="row">
							<div class="col-lg-3 form-group">
								<h5 style="float: left;">Remarks</h5>
								<input name="remarks" class="common-input mb-20 form-control" type="text">
							</div>
							<div class="col-lg-3 form-group">
								<h5 style="float: left;">Upload files</h5>
								<input name="img" class="common-input mb-20 form-control" type="file">
							</div>
							<div class="col-lg-3 form-group">
								<h5 style="float: left;"></h5>
								<button id="submit" type="submit" class="genric-btn primary" style="float: left;margin-top: 20px;">Submit</button>
							</div>
						</div>
						</form>
						<div class="progress-table-wrap">
							<div class="progress-table">
								<div class="table-head">
									<div class="serial">#</div>
									<div class="country">Aksi</div>
									<div class="country">Nama File</div>
									<div class="country">Type</div>
									<div class="country">Remarks</div>
								</div>
                                
                                <?php $no=1; foreach ($transaction_document_details as $key) {
                                
                                ?>

                                <div class="table-row">
									<div class="serial"><?=$no?></div>
									<div class="country"><a target="__blank" href="<?=base_url()?>assets/transactions/document/checkout/<?=$key->name_file?>" class="genric-btn primary circle arrow small">Download</a></div>
									<div class="country"><?=$key->name_file?></div>
									<div class="country"><?=(($key->status == 'raw_customers') ? 'first on registrations' : 'finished')?></div>
									<div class="country"><?=(empty($key->remarks) ? '-' : $key->remarks)?></div>
								</div>
                                <?php $no++; } ?>

							</div>
						</div>
					</div>
                </div>
            </div>

			<div class="whole-wrap">
                <div style="padding: 40px;">
                <div class="section-top-border">
				<?php
				if(!empty($this->session->flashdata('messages'))){ ?>
					<h3 class="mb-30">Info</h3>
						<div class="row">
							<div class="col-lg-12">
								<blockquote class="generic-blockquote"><?=$this->session->flashdata('messages')?></blockquote>
							</div>
						</div><br>
				<?php } ?>
						
						<h3 class="mb-30">List Transaction</h3>
						<div class="progress-table-wrap">
							<div class="progress-table">
								<div class="table-head">
									<div class="serial">#</div>
									<div class="country">Aksi</div>
									<div class="country">TRX CODE</div>
									<div class="country">Paket</div>
									<div class="visit">Total Price</div>
									<div class="percentage">Status Pembayaran</div>
									<div class="percentage">Metode Pembayaran</div>
									<div class="percentage">Status Pengerjaan</div>
									<div class="percentage">Progress</div>
								</div>
                                

                                <div class="table-row">
									<div class="serial">1</div>
									<div class="country"><a href="<?=base_url()?>" class="genric-btn primary circle arrow small">detail</a></div>
									<div class="country"><?=$transaction_details->trx_code?></div>
									<div class="country"><a href="#"><?=$transaction_details->service_name?></a></div>
									<div class="visit">Rp. <?=$transaction_details->base_price?></div>
									<div class="percentage"><?=$transaction_details->status_payment?></div>
									<div class="percentage"><?=(($transaction_details->payments == 'online') ? 'online' : 'di lokasi' )?></div>
									<div class="percentage"><?=$transaction_details->status?></div>
									<div class="percentage">
										<div class="progress">
											<div class="progress-bar color-1" role="progressbar" style="width: 20%;height: 16px;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
                </div>
            </div>
                    