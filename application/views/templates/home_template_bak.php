<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?= base_url()?>assets/home_template/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Medical</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/linearicons.css">
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/bootstrap.css">
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/magnific-popup.css">
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/jquery-ui.css">				
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/nice-select.css">							
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/animate.min.css">
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/owl.carousel.css">			
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/jquery-ui.css">			
			<link rel="stylesheet" href="<?= base_url()?>assets/home_template/css/main.css">
		</head>
		<body>	
		  <header id="header">
	  		<div class="header-top">
	  			<div class="container">
			  		<div class="row align-items-center">
			  			<div class="col-lg-6 col-sm-6 col-4 header-top-left">
			  				<a href="tel:+9530123654896"><span class="lnr lnr-phone-handset"></span> <span class="text"><span class="text">+953 012 3654 896</span></span></a>
				  			<a href="mailto:support@colorlib.com"><span class="lnr lnr-envelope"></span> <span class="text"><span class="text">support@colorlib.com</span></span></a>			
			  			</div>
			  			<div class="col-lg-6 col-sm-6 col-8 header-top-right">
						<?php 
							if($this->session->userdata('user_logged_in') == true) {
								echo '<a href="'.base_url().'" class="primary-btn text-uppercase">'.$this->session->userdata('name').'</a>';
								echo '&nbsp; <a href="'.base_url('login/signout').'" class="primary-btn text-uppercase">Logout</a>';
							} else {
								echo '<a href="'.base_url('login').'" class="primary-btn text-uppercase">Login</a>';
							}
						?>
							
			  			</div>
			  		</div>			  					
	  			</div>
			</div>
		    <div class="container main-menu">
		    	<div class="row align-items-center justify-content-between d-flex">
			      <div id="logo">
			        <a href="index.html"><img src="<?= base_url()?>assets/home_template/img/logo.png" alt="" title="" /></a>
			      </div>
			      <nav id="nav-menu-container">
			        <ul id ="nav-login" class="nav-menu">
			          <li><a href="<?= base_url()?>">Home</a></li>
			          <li><a href="<?= base_url()?>assets/home_template/about.html">About</a></li>
			          <li><a href="<?= base_url()?>assets/home_template/features.html">Features</a></li>
			          <li><a href="<?= base_url()?>assets/home_template/doctors.html">Doctors</a></li>
			          <li><a href="<?= base_url()?>assets/home_template/departments.html">Departments</a></li>
			          <li class="menu-has-children"><a href="">Blog</a>
			            <ul>
			              <li><a href="<?= base_url()?>assets/home_template/blog-home.html">Blog Home</a></li>
			              <li><a href="<?= base_url()?>assets/home_template/blog-single.html">Blog Single</a></li>
			            </ul>
			          </li>	
			          <li class="menu-has-children"><a href="">Pages</a>
			            <ul>
			            	  <li><a href="<?= base_url()?>assets/home_template/elements.html">Elements</a></li>
			            	  <li><a href="#">Item One</a></li>
			            	  <li><a href="#">Item Two</a></li>
					          <li class="menu-has-children"><a href="">Level 2 </a>
					            <ul>
					              <li><a href="#">Item One</a></li>
					              <li><a href="#">Item Two</a></li>
					            </ul>
					          </li>					                		
			            </ul>
			          </li>					          					          		          
			          <li><a href="<?= base_url()?>assets/home_template/contact.html">Contact</a></li>
			        </ul>
			      </nav><!-- #nav-menu-container -->		    		
		    	</div>
		    </div>
		  </header><!-- #header -->


			
            <?php echo $contents; ?>
		
	

			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-2  col-md-6">
							<div class="single-footer-widget">
								<h6>Top Products</h6>
								<ul class="footer-nav">
									<li><a href="#">Managed Website</a></li>
									<li><a href="#">Manage Reputation</a></li>
									<li><a href="#">Power Tools</a></li>
									<li><a href="#">Marketing Service</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-4  col-md-6">
							<div class="single-footer-widget mail-chimp">
								<h6 class="mb-20">Contact Us</h6>
								<p>
									56/8, Santa bullevard, Rocky beach, San fransisco, Los angeles, USA
								</p>
								<h3>012-6532-568-9746</h3>
								<h3>012-6532-568-97468</h3>
							</div>
						</div>							
						<div class="col-lg-6  col-md-12">
							<div class="single-footer-widget newsletter">
								<h6>Newsletter</h6>
								<p>You can trust us. we only send promo offers, not a single spam.</p>
								<div id="mc_embed_signup">
									<form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="form-inline">

										<div class="form-group row" style="width: 100%">
											<div class="col-lg-8 col-md-12">
												<input name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '" required="" type="email">
											</div> 
										
											<div class="col-lg-4 col-md-12">
												<button class="nw-btn primary-btn circle">Subscribe<span class="lnr lnr-arrow-right"></span></button>
											</div> 
										</div>		
										<div class="info"></div>
									</form>
								</div>		
							</div>
						</div>					
					</div>

					<div class="row footer-bottom d-flex justify-content-between">
						<p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						<div class="col-lg-4 col-sm-12 footer-social">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>					
					</div>
				</div>
			</footer>
			<!-- End footer Area -->


			<script src="<?= base_url()?>assets/home_template/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="<?= base_url()?>assets/home_template/js/popper.min.js"></script>
			<script src="<?= base_url()?>assets/home_template/js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
 			<script src="<?= base_url()?>assets/home_template/js/jquery-ui.js"></script>					
  			<script src="<?= base_url()?>assets/home_template/js/easing.min.js"></script>			
			<script src="<?= base_url()?>assets/home_template/js/hoverIntent.js"></script>
			<script src="<?= base_url()?>assets/home_template/js/superfish.min.js"></script>	
			<script src="<?= base_url()?>assets/home_template/js/jquery.ajaxchimp.min.js"></script>
			<script src="<?= base_url()?>assets/home_template/js/jquery.magnific-popup.min.js"></script>	
    		<script src="<?= base_url()?>assets/home_template/js/jquery.tabs.min.js"></script>						
			<script src="<?= base_url()?>assets/home_template/js/jquery.nice-select.min.js"></script>	
			<script src="<?= base_url()?>assets/home_template/js/owl.carousel.min.js"></script>									
			<script src="<?= base_url()?>assets/home_template/js/mail-script.js"></script>	
			<script src="<?= base_url()?>assets/home_template/js/main.js"></script>	
			
			<!--Start of Tawk.to Script-->
			<script type="text/javascript">
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			Tawk_API.visitor = {
			name : '<?=$this->session->userdata('name')?>',
			email : '<?=$this->session->userdata('email')?>'
			};
			(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/60506da0067c2605c0b8d646/1f0t2ofcv';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
			})();
			</script>
			<!--End of Tawk.to Script-->

			<script>
				$(document).ready(function() {
					var session = "<?=$this->session->userdata('user_logged_in')?>";

					if(session == true)
					{
						roleMenu = `
						<li><a href="<?= base_url()?>">Home</a></li>
						<li><a href="<?= base_url()?>transactions">Transaction</a></li>
						<li><a href="<?= base_url()?>">Profile</a></li>
						`;
						$('#nav-login').html(roleMenu);
					}
					
					$('.services-photo').on('change',function(){
						var data = $('.services-photo').find(':selected').data('price');
						var dataDescription = $('.services-photo').find(':selected').data('description');
						
						var foo = dataDescription.replace(/\s/g, '\u00A0');
						$('#price').html('Harga : Rp. '+data)
						$('#description').text(dataDescription);
						$('#input-price').val(data);
					})

					$('#qty').on('keyup',function(){	
						var data = $('.services-photo').find(':selected').data('price');
						if($('#qty').val() == 0 || $('#qty').val() ==  1 || $('#qty').val() == null)
						{
							$('#price').html('Harga : Rp. '+data)
							$('#input-price').val(data);
						} else{
							var price = $('#qty').val();
							var total = price * data;
							$('#price').html('Harga : Rp. '+total)
							$('#input-price').val(total);
						}
					})

					$('#service-paket').on('change', function(){
						if(this.value == 'on_location'){
							
							base_url = "<?=site_url()?>home/submit";
							$("#url-payment-val").val(base_url)

						} else if(this.value == 'online'){
							base_url = "<?=site_url()?>home/checkoutOnline";
							$("#url-payment-val").val(base_url)

						} 
					})

					$('#myFormSave').submit(function(e) {
						e.preventDefault();
						
						if(session == false)
						{
							alert('silahkan login terlebih dahulu sebelum melakukan pemesanan')
							window.location = "<?=base_url()?>login";
						} else {
							$.ajax({
								type: 'POST',
								data: new FormData(this),
								url: $("#url-payment-val").val(),
								// dataType: 'JSON',
								processData: false,
								contentType: false,
								cache: false,
								async: true,
								success: function(data, textStatus, xhr) {
				
									if($('#service-paket').val() == 'online') {
								
										// for payment method midtrans
										snap.pay(data, {
										onSuccess: function(result){
											<?php //$this->session->set_flashdata('messages','Transaksi pemesanan anda sukses.');?>
											window.location = "<?=base_url()?>transactions";
											// alert('payment success')
											
										},
										onPending: function(result){
											<?php //$this->session->set_flashdata('messages','Transaksi pemesanan anda pending. segera lakukan pembayaran agar dapat di segera di proses ke tahap selanjutnya');?>
											window.location = "<?=base_url()?>transactions";
											// alert('payment pending')

										},
										onError: function(result){
											alert('payment failed')
										}
										});
										
									} else {
										window.location = "<?=base_url()?>transactions";
										// alert('success on location')
									}

								},
								error: function (request, status, error) {
									var obj = JSON.parse(request.responseText);
									alert(obj.error_message);
								}
							})
						}

						
					})
					
				})
			</script>

		</body>
	</html>