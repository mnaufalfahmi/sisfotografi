<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sisfotografi &mdash; Teknik Informatika </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="freehtml5.co" />
  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Select 2 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/select2/select2.min.css">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
	<!-- Pace style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/pace/pace.min.css">

	<!-- DataTables -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/dashboard_template/adminLTE/plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/dist/css/skins/_all-skins.min.css">

      <!-- [favicon] begin -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/logo/Logo.jpg" />

    <link rel="icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/logo/Logo.jpg" />
    <!-- [favicon] end -->

    <!-- Touch icons more info: http://mathiasbynens.be/notes/touch-icons -->
    <!-- For iPad3 with retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/img/logo/Logo.jpg" />
    <!-- For first- and second-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/img/logo/Logo.jpg" />
    <!-- For first- and second-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/img/logo/Logo.jpg">
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/img/logo/Logo.jpg" />

		<!-- jQuery 2.2.3 -->
	  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>

	</head>
	<body class="hold-transition skin-green sidebar-mini">

		<header class="main-header">
		  <!-- Logo -->
		  <a href=" <?php echo base_url('dashboard'); ?>" class="logo">
		    <!-- mini logo for sidebar mini 50x50 pixels -->
		    <span class="logo-mini"><b>S</b>F</span>
		    <!-- logo for regular state and mobile devices -->
		    <span class="logo-lg"><b>Sisfotografi</b></span>
		  </a>

		  <!-- Header Navbar: style can be found in header.less -->
		  <nav class="navbar navbar-static-top">
		    <!-- Sidebar toggle button-->
		    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		      <span class="sr-only">Toggle navigation</span>
		    </a>
		    <!-- Navbar Right Menu -->
		    <div class="navbar-custom-menu">
		      <ul class="nav navbar-nav">

						<li class="dropdown user user-menu">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		            <img src="
						<?php
							if($this->session->userdata('gender')=="male") {
								echo base_url()."assets/img/template/default_boy.png";
							}else {
								echo base_url()."assets/img/template/default_girl.png";
							}
							?>" class="user-image" alt="User Image">
		            <span class="hidden-xs"><?php echo $this->session->userdata('name'); ?></span>
		          </a>
		          <ul class="dropdown-menu">
		            <!-- User image -->
		            <li class="user-header">
		              <img src="<?php
							if($this->session->userdata('gender')=="male") {
								echo base_url()."assets/img/template/default_boy.png";
							}else {
								echo base_url()."assets/img/template/default_girl.png";
							}
							?>" class="img-circle" alt="User Image">

		              <p>
		                <?php echo $this->session->userdata('name').' - '.$this->session->userdata('role'); ?>
		              </p>
		            </li>

		            <!-- Menu Footer-->
								<li class="user-footer">
									
									<div class="pull-right">
										<a href="<?php echo base_url('login/signout')?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
		          </ul>
		        </li>
		        <!-- Control Sidebar Toggle Button -->
		        <li>
		          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
		        </li>
		      </ul>
		    </div>

		  </nav>
		</header>

		<aside class="main-sidebar">
	    <!-- sidebar: style can be found in sidebar.less -->
	    <section class="sidebar">
	      <!-- Sidebar user panel -->
	      <div class="user-panel">
	        <div class="pull-left image">
	          <img style="height:47px;width:50px;"src="
			  <?php
				if($this->session->userdata('gender')=="male") {
					echo base_url()."assets/img/template/default_boy.png";
				}else {
					echo base_url()."assets/img/template/default_girl.png";
				}
				?>" class="img-circle" alt="User Image">
	        </div>
	        <div class="pull-left info">
	          <p><?php echo $this->session->userdata('name'); ?></p>
	          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	        </div>
	      </div>
	      <!-- search form -->
	      <?php //echo form_open('dashboard/search','class="sidebar-form"'); ?>
	        <!-- <div class="input-group">
	          <input type="text" name="q" class="form-control" placeholder="Cari Konten">
	              <span class="input-group-btn">
	                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
	                </button>
	              </span>
	        </div>
	      </form> -->
	      <!-- /.search form -->
	      <!-- sidebar menu: : style can be found in sidebar.less -->

		  <ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
				<li <?php if($this->uri->segment(1) == 'dashboard'){echo 'class="active"'; }else { echo ''; } ?>>
				<a href="<?php echo base_url()?>dashboard">
					<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				</a>
				</li>
				<li <?php if($this->uri->segment(1) == 'transactions'){echo 'class="active"'; }else { echo ''; } ?>>
				<a href="<?php echo base_url()?>transactions/all">
					<i class="fa fa-list"></i> <span>Transactions</span>
				</a>
				</li>
				<li <?php if($this->uri->segment(1) == 'customers'){echo 'class="active"'; }else { echo ''; } ?>>
				<a href="<?php echo base_url()?>customers">
					<i class="fa fa-users"></i> <span>Customers</span>
				</a>
				</li>

				<!-- <li class="treeview">
					<a href="#">
						<i class="fa fa-list"></i>
						<span>Services</span>
						<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu" style="display: none;">
						<li><a href="<?php echo base_url()?>services"><i class="fa fa-circle-o"></i> Services</a></li>
						<li><a href="<?php echo base_url()?>services/category"><i class="fa fa-circle-o"></i> Services Category</a></li>
					</ul>
				</li> -->

				<li <?php if($this->uri->segment(1) == 'services' && $this->uri->segment(2) != 'category'){echo 'class="active"'; }else { echo ''; } ?>>
				<a href="<?php echo base_url()?>services">
					<i class="fa fa-list"></i> <span>Services</span>
				</a>
				</li>
				<li <?php if($this->uri->segment(2) == 'category'){echo 'class="active"'; }else { echo ''; } ?>>
				<a href="<?php echo base_url()?>services/category">
					<i class="fa fa-list"></i> <span>Services Category</span>
				</a>
				</li>
			
		</ul>

	    </section>
	    <!-- /.sidebar -->
	  </aside>

  <!-- Contents -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Dashboard
				<small> <?php echo $this->session->userdata('role');?></small>
			</h1>
			<!-- </section> -->
		<!-- Main content -->
		<!-- /.content -->
			<?php echo $contents; ?>
	</div>
		<!-- /.content-wrapper -->

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 1.0.0
		</div>
		<strong>Copyright &copy; <?php echo Date('Y'); ?> <a href="#">Sisfotografi Apps - Teknik Informatika</a></strong> All rights
		reserved.
	</footer>
	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Create the tabs -->
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs"></ul>
		<!-- Tab panes -->
		<div class="tab-content">
		<div class="tab-pane" id="control-sidebar-home-tab">
		</div>
	</aside>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
			 immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
	
	<!-- <script src="https://okbabe-production-cluster.firebaseapp.com/__/firebase/5.7.2/firebase-app.js"></script>
    <script src="https://okbabe-production-cluster.firebaseapp.com/__/firebase/5.7.2/firebase-messaging.js"></script>
    <script src="https://okbabe-production-cluster.firebaseapp.com/__/firebase/init.js"></script> -->

	<script>
		function allowAlphaNumericSpace(e) {
			var code = ('charCode' in e) ? e.charCode : e.keyCode;
			if (!(code == 32) &&
			!(code > 47 && code < 58) && // numeric (0-9)
			!(code > 64 && code < 91) && // upper alpha (A-Z)
			!(code > 96 && code < 123)) { // lower alpha (a-z)
			e.preventDefault();
			}
		}
		function allowAlphaNumericSpace_(e) {
			var code = ('charCode' in e) ? e.charCode : e.keyCode;
			if ((code == 32)) { // lower alpha (a-z)
			e.preventDefault();
			}
		}
	</script>

	<script>
		var baseurl 		 = '<?php echo base_url();?>';
        var apiKey           = '<?php echo $this->config->item('apiKey') ?>';
        var authDomain       = '<?php echo $this->config->item('authDomain') ?>';
        var databaseURL      = '<?php echo $this->config->item('databaseURL') ?>';
        var projectId        = '<?php echo $this->config->item('projectId') ?>';
        var storageBucket    = '<?php echo $this->config->item('storageBucket') ?>';
        var messagingSenderId= '<?php echo $this->config->item('messagingSenderId') ?>';
        var appId            = '<?php echo $this->config->item('appId') ?>';
		var measurementId    = '<?php echo $this->config->item('measurementId') ?>';
    </script>

	<!-- <script src="https://www.gstatic.com/firebasejs/7.8.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.8.0/firebase-storage.js"></script> -->
<script>

var firebaseConfig = {
    apiKey: "AIzaSyA2FfRR3JnvUaZVzMDhYThCIPSNomUlS-Q",
    authDomain: "data-77204.firebaseapp.com",
    databaseURL: "https://data-77204.firebaseio.com",
    projectId: "data-77204",
    storageBucket: "data-77204.appspot.com",
    messagingSenderId: "278474216595",
    appId: "1:278474216595:web:9381be31f2d9655d452966"
    // measurementId: "G-LMTVMHQ48G"
  };
  // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);

  
</script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/bootstrap/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/dist/js/app.min.js"></script>
  <!-- Sparkline -->
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/sparkline/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- ChartJS 1.0.1 -->
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/chartjs/Chart.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/dist/js/demo.js"></script>
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/dashboard_template/adminLTE/plugins/select2/select2.full.min.js"></script>
	<!-- DataTables -->
	<script src="<?php echo base_url();?>assets/dashboard_template/adminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>assets/dashboard_template/adminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- PACE -->
  <script src="<?php echo base_url();?>assets/dashboard_template/adminLTE/plugins/pace/pace.min.js"></script>
	<!-- Input Mask -->
	<script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/input-mask/jquery.inputmask.js"></script>
	<script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="<?php echo base_url(); ?>assets/dashboard_template/adminLTE/plugins/input-mask/jquery.inputmask.extensions.js"></script>

	<script src="<?php echo base_url(); ?>assets/dashboard_template/ckeditor/ckeditor.js"></script>

    <script>
	
	baseurl = "<?php echo base_url()?>";
    $(function () {
      //Initialize Select2 Elements
        $(".select2").select2();
				$("[data-mask]").inputmask();
      //Date picker
      // $('#datepicker').datepicker({
      // format: "yyyy-mm-dd",
      // autoclose: true
      // });
      // $('#datepicker2').datepicker({
      // format: "yyyy-mm-dd",
      // autoclose: true
      // });
      //Timepicker
      // $(".timepicker").timepicker({
      // showInputs: false
      //   });
     });
    </script>
	</body>
</html>
