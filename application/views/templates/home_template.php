<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Sistem Fotografi</title>
    <link href="<?=base_url()?>assets/home_template/Eshopper/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/home_template/Eshopper/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/home_template/Eshopper/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/home_template/Eshopper/css/price-range.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/home_template/Eshopper/css/animate.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/home_template/Eshopper/css/main.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/home_template/Eshopper/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="<?=base_url()?>assets/home_template/Eshopper/js/html5shiv.js"></script>
    <script src="<?=base_url()?>assets/home_template/Eshopper/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?=base_url()?>assets/img/logo//Logo.jpg">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url()?>assets/home_template/Eshopper/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url()?>assets/home_template/Eshopper/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url()?>assets/home_template/Eshopper/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?=base_url()?>assets/home_template/Eshopper/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="<?=base_url()?>assets/home_template/Eshopper/#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="<?=base_url()?>assets/home_template/Eshopper/#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="<?=base_url()?>assets/home_template/Eshopper/#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?=base_url()?>assets/home_template/Eshopper/#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?=base_url()?>assets/home_template/Eshopper/#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="<?=base_url()?>assets/home_template/Eshopper/#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="<?=base_url()?>assets/home_template/Eshopper/#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="<?=base_url()?>"><img src="<?=base_url()?>assets/img/logo//Logo.jpg" alt="" style="width: 143px;"/></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<?php
								if($this->session->userdata('user_logged_in') == true){?>
									<li><a href="<?=base_url()?>"><i class="fa fa-user"></i> Account</a></li>
									<li><a href="<?=base_url()?>transactions"><i class="fa fa-exchange"></i> Transactions</a></li>
									<li><a href="<?=base_url()?>login/signout"><i class="fa fa-lock"></i> Logout</a></li>
								<?php } else { ?>
									<li><a href="<?=base_url()?>login"><i class="fa fa-lock"></i> Login</a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="<?=base_url()?>" <?=(($this->uri->segment(2) == 'home' || $this->uri->segment(2) == '') ? 'class="active"' : '' )?>>Home</a></li>
								<li><a href="<?=base_url()?>home/contact" <?=(($this->uri->segment(2) == 'contact') ? 'class="active"' : '' )?> >Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<!-- <input type="text" placeholder="Search"/> -->
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	

	<?php echo $contents; ?>

	<br><br><br><br><br><br>
	<footer id="footer"><!--Footer-->
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © <?=date('Y')?> tokyo studio Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	
	<script type="text/javascript">
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			Tawk_API.visitor = {
			name : '<?=$this->session->userdata('name')?>',
			email : '<?=$this->session->userdata('email')?>'
			};
			(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/60506da0067c2605c0b8d646/1f0t2ofcv';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
			})();
	</script>

  
    <script src="<?=base_url()?>assets/home_template/Eshopper/js/jquery.js"></script>
	<script src="<?=base_url()?>assets/home_template/Eshopper/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/home_template/Eshopper/js/jquery.scrollUp.min.js"></script>
	<script src="<?=base_url()?>assets/home_template/Eshopper/js/price-range.js"></script>
    <script src="<?=base_url()?>assets/home_template/Eshopper/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/home_template/Eshopper/js/main.js"></script>
</body>
</html>