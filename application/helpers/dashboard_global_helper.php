<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function authentication($context)
{
    // !IMPORTANT FOR DASHBOARD LOGIN !
    if($context->session->userdata('status')!="login")
    {
      echo "<script>
      alert('Silahkan Login terlebih dahalu !');
      window.location.href='".base_url('login')."';
      </script>";
    }
    $context->load->model("model_home");
    // !IMPORTANT FOR DASHBOARD LOGIN!

}

function dashboard_data($context)
{
  // !IMPORTANT FOR DASHBOARD TEMPLATE!
  
  $data['id']       = $context->session->userdata('id');
  $data['role']     = $context->session->userdata('role');
  $data['name']     = $context->session->userdata('name');
  $data['gender']   = $context->session->userdata('gender');
  $data['username'] = $context->session->userdata('username');
  $data['email']    = $context->session->userdata('email');
  $data['status']   = $context->session->userdata('status');
  $data['telp']   = $context->session->userdata('telp');

  // if($context->session->userdata('role') == 'customers') {
  //   echo "<script>
  //     alert('anda tidak diberi akses untuk membuka halaman ini!');
  //     window.location.href='".base_url()."';
  //     </script>";
  // }
  
  return $data;
}

function secure_bypass_menu($context)
{
  // $context->load->model("model_navigation_roles");
  // if($context->model_navigation_roles->secure_menu($context)->num_rows()>0)
  // {
  //   return true;
  // }
  // else
  // {
  //   echo "<script>
  //   alert('Maaf anda tidak di izinkan menggunakan fitur ini.');
  //   window.location.href='".base_url('dashboard')."';
  //   </script>";
  //   return false;
  // }
}

function do_create_log($context,$did)
{
  $context->load->model("model_dashboard");
  $context->model_dashboard->create_log($context,$did);
}


function tanggal_indo($tanggal, $cetak_hari = false)
{
  $hari = array (
  'Sun' => 'Minggu',
    'Mon' => 'Senin',
    'Tue' => 'Selasa',
    'Wed' => 'Rabu',
    'Thu' => 'Kamis',
    'Fri' => 'Jumat',
    'Sat' => 'Sabtu'
      );
      
  $bulan = array (1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
      );
  $split    = explode('-', $tanggal);
  $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
  
  if ($cetak_hari) {
    //$num = date('N', strtotime($tanggal));
    $day = date('D', strtotime($tanggal));
    return $hari[$day] . ', ' . $tgl_indo;
  }
  return $tgl_indo;
}
