<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model
{
  public function __construct()
  {
      parent::__construct();
  }

  public function counting()
  {
    $progress = $this->db->get_where('tbl_transactions',array('deleted'=>0,'status'=>'progress'))->num_rows();
    $success  = $this->db->get_where('tbl_transactions',array('deleted'=>0,'status'=>'success'))->num_rows();
    $failed   = $this->db->get_where('tbl_transactions',array('deleted'=>0,'status'=>'failed'))->num_rows();
    $canceled = $this->db->get_where('tbl_transactions',array('deleted'=>0,'status'=>'canceled'))->num_rows();

    $data = array(
      'progress' => $progress,
      'success'  => $success,
      'failed'  => $failed,
      'canceled'  => $canceled
    );

    return $data;
     
  }
}
