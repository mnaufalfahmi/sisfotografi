<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model
{
  function verification($user_login)
  {
    return $this->db->get_where("tbl_users",$user_login);
  }

  function get_veified_user_data($email)
  {
    $this->db->select('tbl_users.*')->from('tbl_users')->where('email',$email);
    $data = $this->db->get()->result_array();
    return $data;
  }

}
