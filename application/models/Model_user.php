<?php

class Model_user extends CI_Model
{

  function get_user($param=array())
  {
    if(count($param)<=0)
    {
      $query = "SELECT
              user.id_user,
              user.nik,
              user.name_user,
              user.bio_user,
              user.jenis_kelamin,
              user.telp_user,
              user.email_user,
              user.image_user,
              user.facebook,
              user.linkedin,
              user.google_plus,
              user.username,
              user.password,
              user.tgl_dibuat,
              user_roles.id_role,
              user_roles.role_name,
              user_roles.role_desc,
              lab.*
              FROM user
              INNER JOIN user_roles ON user.id_role = user_roles.id_role
              INNER JOIN lab ON user.id_lab = lab.id_lab
              ORDER BY user.id_user ASC";
    return $this->db->query($query);
    }
    else
    {
      return $this->db->get_where("user",$param);
    }
  }


  function input()
  {
    //Validate
    $username=$this->input->post('username');
    $name_user=$this->input->post('name_user');

    $config['upload_path'] = './assets/img/user/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $this->load->library('upload',$config);
    $this->upload->do_upload();
    $hasil  = $this->upload->data();

    //fix error wrong extension
    $file_url=FCPATH.'assets/img/user/'.$hasil['file_name'];

    if(is_file($file_url)!=1)
    {
      $param=array("name_user"=>$name_user);
      $this->db->where($param);
      $this->db->delete("user");
      $result=true;
      echo "<script>
      alert('Pastikan semua field terisi !.');
      window.location.href='".base_url('user/input')."';
      </script>";
    }
    else
    {
        $file_ext= explode('.',$hasil['file_name']);
        $file_rename=FCPATH.'assets/img/user/profil-'.$name_user.'.'.$file_ext[1];
        rename($file_url,$file_rename);
        $data = array(
                  'name_user'      => $this->input->post('name_user'),
                  'nik'      => $this->input->post('nik'),
                  'jenis_kelamin'  => $this->input->post('jenis_kelamin'),
                  'telp_user'      => $this->input->post('telp_user'),
                  'bio_user'       => $this->input->post('bio_user'),
                  'email_user'     => $this->input->post('email_user'),
                  'facebook'       => $this->input->post('facebook'),
                  'google_plus'    => $this->input->post('google_plus'),
                  'linkedin'       => $this->input->post('linkedin'),
                  'username'       => $username,
                  'password'       => md5($this->input->post('password')),
                  'image_user'     => 'profil-'.$name_user.'.'.$file_ext[1],
                  'id_role'        => $this->input->post('id_role'),
                  'id_lab'        => $this->input->post('id_lab'));

        $result=$this->db->insert('user',$data);

        if($this->db->trans_status()===true)
        {
          echo "
          <script>
          alert('User berhasil di input.');
          window.location.href='".base_url('user')."';
          </script>";
          $this->db->trans_commit();
        }else
        {
          $file_delete=FCPATH."assets/img/user/".'profil-'.$name_user.'.'.$file_ext[1];
          if(is_file($file_delete)==1)
          {
            unlink($file_delete);
          }

          echo "
          <script>
          alert('User gagal di input, username atau nama user telah ada.');
          window.location.href='".base_url('user/input')."';
          </script>";
          $this->db->trans_rollback();
        }

    }

    return $result;
  }

  function get_old_image_user_name($id)
  {
    $param=array("id_user"=>$id);
    $data=$this->db->get_where("user",$param)->result_array();
    foreach ($data as $key)
    {
      $ret=$key['image_user'];
    }
    return $ret;
  }

function edit()
  {
    
    $username=$this->input->post('username');
    $name_user=$this->input->post('name_user');

    $config['upload_path']    = "./assets/img/user/";
    $config['allowed_types']  = "gif|jpg|png|jpeg";
    $this->load->library('upload',$config);
    $this->upload->do_upload();

    $hasil  = $this->upload->data();

    //fix error wrong extension
    $file_url=FCPATH.'assets/img/user/'.$hasil['file_name'];

    if($this->input->post('password')!=NULL || $this->input->post('password')!="")
    {
      if($hasil['file_name']!=NULL || $hasil['file_name']!="")
      {
        if(is_file($file_url)!=1)
        {
          $param=array("name_user"=>$name_user);
          $this->db->where($param);
          $this->db->delete("user");

          $url=base_url('user/edit/'.$this->uri->segment(3));

          echo "<script>
          alert('Pastikan semua field terisi !.');
          window.location.href='".$url."';
          </script>";
        }

        else
        {
          unlink("assets/img/user/".$this->input->post('image_user'));
          $file_ext= explode('.',$hasil['file_name']);
          $file_rename=FCPATH.'assets/img/user/profil-'.$name_user.'.'.$file_ext[1];
          rename($file_url,$file_rename);
          $data = array(
                  'name_user'      => $name_user,
                  'nik'      => $this->input->post('nik'),
                  'jenis_kelamin'  => $this->input->post('jenis_kelamin'),
                  'telp_user'      => $this->input->post('telp_user'),
                  'bio_user'       => $this->input->post('bio_user'),
                  'email_user'     => $this->input->post('email_user'),
                  'facebook'       => $this->input->post('facebook'),
                  'google_plus'    => $this->input->post('google_plus'),
                  'linkedin'       => $this->input->post('linkedin'),
                  'username'       => $username,
                  'password'       => md5($this->input->post('password')),
                  'image_user'     => 'profil-'.$name_user.'.'.$file_ext[1],
                  'id_role'        => $this->input->post('id_role'),
                  'id_lab'        => $this->input->post('id_lab'));

        }
      }
      else
      {
        $data = array(
                  'name_user'      => $this->input->post('name_user'),
                  'nik'      => $this->input->post('nik'),
                  'jenis_kelamin'  => $this->input->post('jenis_kelamin'),
                  'telp_user'      => $this->input->post('telp_user'),
                  'bio_user'       => $this->input->post('bio_user'),
                  'email_user'     => $this->input->post('email_user'),
                  'facebook'       => $this->input->post('facebook'),
                  'google_plus'    => $this->input->post('google_plus'),
                  'linkedin'       => $this->input->post('linkedin'),
                  'username'       => $this->input->post('username'),
                  'password'       => md5($this->input->post('password')),
                  'id_role'        => $this->input->post('id_role'),
                  'id_lab'        => $this->input->post('id_lab'));
    }
  }
    else
    {
      if($hasil['file_name']!=NULL || $hasil['file_name']!="")
      {
        if(is_file($file_url)!=1)
        {
          $param=array("name_user"=>$name_user);
          $this->db->where($param);
          $this->db->delete("user");

          echo "<script>
          alert('Pastikan semua field terisi !.');
          window.location.href='".base_url('user/input')."';
          </script>";
        }
        else
        {
          unlink("assets/img/user/".$this->input->post('image_user'));
          $file_ext= explode('.',$hasil['file_name']);
          $file_rename=FCPATH.'assets/img/user/profil-'.$name_user.'.'.$file_ext[1];
          rename($file_url,$file_rename);
          $data = array(
                  'name_user'      => $name_user,
                  'nik'      => $this->input->post('nik'),
                  'jenis_kelamin'  => $this->input->post('jenis_kelamin'),
                  'telp_user'      => $this->input->post('telp_user'),
                  'bio_user'       => $this->input->post('bio_user'),
                  'email_user'     => $this->input->post('email_user'),
                  'facebook'       => $this->input->post('facebook'),
                  'google_plus'    => $this->input->post('google_plus'),
                  'linkedin'       => $this->input->post('linkedin'),
                  'username'       => $username,
                  'image_user'     => 'profil-'.$name_user.'.'.$file_ext[1],
                  'id_role'        => $this->input->post('id_role'),
                  'id_lab'        => $this->input->post('id_lab'));

        }
      }
      else
      {
        $data = array(
                  'name_user'      => $this->input->post('name_user'),
                  'nik'      => $this->input->post('nik'),
                  'jenis_kelamin'  => $this->input->post('jenis_kelamin'),
                  'telp_user'      => $this->input->post('telp_user'),
                  'bio_user'       => $this->input->post('bio_user'),
                  'email_user'     => $this->input->post('email_user'),
                  'facebook'       => $this->input->post('facebook'),
                  'google_plus'    => $this->input->post('google_plus'),
                  'linkedin'       => $this->input->post('linkedin'),
                  'username'       => $this->input->post('username'),
                  'id_role'        => $this->input->post('id_role'),
                  'id_lab'        => $this->input->post('id_lab'));
       }
    }

    $this->db->where('id_user', $this->input->post('id'));
    $result=$this->db->update('user',$data);
    return $result;

  }

  function delete($param="")
  {
    $file_delete=FCPATH."assets/img/user/".$this->get_old_image_user_name($this->uri->segment(3));
    if(is_file($file_delete)==1)
    {
      unlink($file_delete);
    }
    if($param!="")
    {
      $this->db->where($param);
    }
    $this->db->delete('user');

  }

  function get_lab()
  {
    $data = $this->db->get('lab');
    return $data;
  }


}
