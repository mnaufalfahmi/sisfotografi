<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_transactions extends CI_Model
{

  protected $table         = 'tbl_transactions';
  protected $key           = 'id';
  protected $date_format   = 'datetime';
  protected $soft_deletes  = TRUE;

  protected $column_order  = array(null,'trx_code','status','base_price','name_user','telp_user','service_code','service_name','payments','remarks','created_on'); //set column field database for datatable orderable
  protected $column_search = array('tbl_transactions.trx_code','tbl_transactions.status','tbl_transactions.base_price','tbl_transactions.name_user','tbl_transactions.telp_user','tbl_transactions.service_code','tbl_transactions.service_name','tbl_transactions.payments','tbl_transactions.remarks','tbl_transactions.created_on'); //set column field database for datatable searchable 
  protected $order         = array('created_on' => 'DESC'); // default order 

  public function __construct()
  {
      parent::__construct();
  }

  public function _get_datatables_query()
  {
      $this->db->select($this->table.'.*');
      $this->db->from($this->table);
      $this->db->where('deleted','0');

      $i = 0;
   
      foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

      //deleted = 0
      $this->db->where($this->table.'.deleted', 0);
      $this->db->order_by('id','DESC');
       
      if(isset($_POST['order'])) // here order processing
      {
          $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      } 
      else if(isset($this->order))
      {
          $order = $this->order;
          $this->db->order_by(key($order), $order[key($order)]);
      }
  }

  public function get_datatables()
  {
      $this->_get_datatables_query();

      if($_POST['length'] != -1)
          $this->db->limit($_POST['length'], $_POST['start']);

      $query = $this->db->get();
      return $query->result();
  }

  public function count_filtered()
  {
      $this->_get_datatables_query();
      $query = $this->db->get();
      return $query->num_rows();
  }

  public function count_all()
  {
      $this->db->from($this->table);
      $this->db->where($this->table.'.deleted', 0);
      
      return $this->db->count_all_results();
  }

  public function get_transaction_documents()
  {
      $this->db->select('a.*,b.status as document_status, b.path_file')
        ->from('tbl_transactions a')
        ->join('tbl_transaction_documents b','a.trx_code = b.trx_code','left')
        ->join('tbl_users c','a.id_user = c.id','left')
        ->where('a.deleted',0)
        // ->where('b.status','finished')
        ->where('a.id_user',$this->session->userdata('id'))
        ->group_by('a.id');
        $data = $this->db->get()->result();
        return $data;
  }

}
